<?php
	return [


		//home page
		'home_btn'		=> 'الرئيسية',		
		'search_btn'	=> 'بحث',		
		'select_location'=> 'المكان',		
		'price_from'	=> 'السعر من',		
		'price_to'	=> 'السعر الى',
		'all_units_btn'	=> 'كل الوحدات',				
		'units' 		=> 'الوحدات',
		'services' 		=> 'خدماتنا',
		'about' 		=> 'عنا',
		'contact'		=> 'اتصل بنا',
		'most_popular' 	=> 'وحدات مميزة',
		'partners' 		=> 'شركائنا',


		//all units
		'all_units_title' 	=> 'احدث الوحدات',
		'recent_units' 		=> 'احدث الوحدات',
		'size' 				=> 'المساحة بالمتر',
		'price' 			=> 'السعر',
		'read_more'     	=> 'المزيد',
		'no_units_found'	=> 'لا يوجد وحدات توافق بحثك',


		//show unit
		'unit_latest_units' 	=> 'اخر الوحدات',
		'unit_description'		=> 'معلومات اكثر',
		'unit_related_title'	=> 'وحداث مشابهة',
		'read_more_unit'    	=> 'المزيد',
		'unit_type'				=> 'معروض ل',
		'unit_section'			=> 'نوع الاعلان',
		'unit_location'			=> 'المكان',
		'unit_floor'			=> 'الدور - الأدوار',
		'unit_view'				=> 'تطل على ',
		'unit_rooms'			=> 'الغرف',
		'unit_bathrooms'		=> 'الحمامات',
		'unit_price'			=> 'السعر',
		'unit_pay_type'			=> 'طريقة السداد',
		'unit_build_year'		=> 'سنة البناء',
		'unit_delivery_date'	=> 'تاريخ التسليم',
		'unit_phone'			=> 'الهاتف',
		'unit_address'			=> 'العنوان',
		'contact_btn'			=> 'طلب اتصال',
		'unit_contact_head' 	=> 'طلب اتصال',
		'unit_contact_name' 	=> 'الاسم',
		'unit_contact_phone' 	=> 'رقم التليفون',
		'unit_contact_email' 	=> 'الايميل ان وجد',
		'unit_contact_message' 	=> 'الرسالة',
		'unit_contact_send' 	=> 'ارسال',
		'unit_applied_msg' 		=> 'سيتم التواصل معك فى اقرب وقت',
		'address'				=> 'العنوان',
		'phone'					=> 'الهاتف',
		'send_msg'				=> 'ارسل لنا',


		//contact page
		'contact_h1'			=> 'تواصل معنا',		
		'contact_address_h1'	=> 'العنوان',
		'contact_phone_h1'		=> 'الهاتف',
		'contact_name_pl'		=> 'الاسم',
		'contact_phone_pl'		=> 'الهاتف',
		'contact_email_pl'		=> 'الايميل',
		'contact_message_pl'	=> 'رسالتك',
		'contact_submit_Btn'	=> 'ارسال',
	];
