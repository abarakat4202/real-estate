<?php

return [

		//navbar and sidebar
		'dashboard' 			=> 'لوحة التحكم',
		'welcome' 				=> 'مرحبا',
		'settings' 				=> 'الاعدادات',
		'global_settings' 		=> 'عام',
		'slider' 				=> 'المستعرض',
		'most_popular' 			=> 'وحدات مميزة',
		'about' 				=> 'عنا',
		'units'					=> 'الوحدات',
		'add_unit'				=> 'اضافة وحدة',
		'all_unit'				=> 'كل الوحدات',
		'users'					=> 'الأعضاء',
		'add_user'				=> 'اضافة عضو',
		'all_users'				=> 'كل الأعضاء',
		'services'				=> 'الخدمات',
		'add_service'			=> 'اضافة خدمة',
		'all_services'			=> 'كل الخدمات',
		'inquires'				=> 'استفسارات العملاء',
		'sms_send'				=> 'رسائل SMS',		
		'new_messages'			=> 'رسائل حديثة',
		'view_messages'			=> 'مشاهدة كل الرسائل',
		'new_alerts'			=> 'طلبات الاتصال من العملاء',
		'view_alerts'			=> 'مشاهدة كل الطلبات',
		'logout_btn'			=> 'تسجيل الخروج',
		'msg_heading'			=> 'الرسالة',
		'about_header'			=> 'عنا',

		//logout modal
		'logout_header'			=> 'تأكيد تسجيل الخروج',
		'logout_msg'			=> 'هل ترغب فى تسجيل خروجك؟',
		'cancel'				=> 'الغاء',

		//comomn varaibles
		'en_title'				=> 'العنوان بالإنجليزية',
		'ar_title'				=> 'العنوان بالعربية',
		'en_des'				=> 'الوصف بالانجليزية',
		'ar_des'				=> 'الوصف بالعربية',
		'image'					=> 'الصورة',
		'save_btn'				=> 'حفظ',
		'submit_btn'			=> 'حفظ',
		'date_lbl'				=> 'التاريخ',
		'service_lbl'			=> 'اسم الخدمة',
		'unit_lbl'				=> 'عتوان الوحدة',
		'user_lbl'				=> 'اسم العضو',
		'added_by_lbl'			=> 'تمت الاضافة بواسطة',
		'actions_lbl'			=> 'اجراءات',
		'name_lbl'				=> 'الاسم',
		'phone_lbl'				=> 'الهاتف',
		'email_lbl'				=> 'الايميل',
		'message_lbl'			=> 'الرسالة',
		'actions_lbl'			=> 'اجراءات',
		'edit_btn'				=> 'تعديل',
		'delete_btn'			=> 'حذف',
		'show_btn'				=> 'مشاهدة',
		'search_statement'		=> 'ابحث...',

		//global settings 
		'global_header'			=> 'الاعدادات العامة',
		'website_ar_title'		=> 'اسم الموقع بالعربية',
		'website_en_title'		=> 'اسم الموقع بالانجليزية',
		'website_phone'			=> 'الهاتف',
		'website_ar_address'	=> 'عنوان الشركة بالعربية',
		'website_en_address'	=> 'عتوان الشركة بالانجليزية',
		'website_logo'			=> 'صورة اللوجو',
		'fb_page'				=> 'لينك صفحة Facebbok',
		'slogan_en_title'		=> 'الشعار بالإنجليزية',
		'slogan_ar_title'		=> 'الشعار بالعربية',
		'slogan_ar_des'			=> 'وصف الشعار بالعربية',
		'slogan_en_des'			=> 'وصف الشعار بالإنجليزية',
		'slogan_img'			=> 'خلفية الشعار',
		'partner_1'				=> 'الشريك الاول',
		'partner_2'				=> 'الشريك الثانى',
		'partner_3'				=> 'الشريك الثالث',
		'partner_4'				=> 'الشريك الرابع',
		'partner_5'				=> 'الشريك الخامس',

		//sliders
		'slider_header'			=> 'المستعرض',
		'slider1'				=> 'المستعرض الاول',
		'slider2'				=> 'المستعرض الثانى',
		'slider3'				=> 'المستعرض الثالث',

		//delete modal
		'del_header'			=> 'حذف عنصر؟',
		'del_msg'				=> 'هل انت متأكد من حذف هذا العنصر',
		'cancel'				=> 'الغاء',
		'confirm'				=> 'نعم',
		'not_confirm'			=> 'لا',

		//message modal
		'message_heading'		=> 'الرسالة',

		//login page
		'login_header'			=> 'تسجيل الدخول',
		'login_id_lbl'			=> 'الهاتف او الايميل',
		'login_pass_lbl'		=> 'كلمة السر',
		'remember_lbl'			=> 'تذكرنى',
		'login_btn'				=> 'تسجيل الدخول',

		//most_popular
		'unit_1'				=> 'الوحدة الاولى',
		'unit_2'				=> 'الوحدة الثانية',
		'unit_3'				=> 'الوحدة الثالثة',

		//service
		'icon'					=> 'الرمز',
		'service_header'		=> 'خدمة',

		//add unit
		'unit_header'			=> 'وحدة',
		'unit_title'			=> 'اسم الوحدة',
		'unit_des'				=> 'الوصف',
		'unit_price'			=> 'السعر',
		'unit_size'				=> 'المساحة بالمتر',
		'unit_type'				=> 'نوع الوحدة',
		'unit_section'			=> 'القسم',
		'unit_rooms'			=> 'عدد الغرف',
		'unit_bathrooms'		=> 'عدد الحمامات',
		'unit_floor'			=> 'الدور - عدد الادوار',
		'unit_view'				=> 'تطل على',
		'unit_build_year'		=> 'سنة البناء',
		'unit_delivery_date'	=> 'تاريخ التسليم',
		'unit_pay_type'			=> 'طريقة السداد',
		'unit_loaction'			=> 'المكان',
		'unit_address'			=> 'العنوان',

		//add user
		'user_header'			=> 'الاعضاء',
		'user_name'				=> 'الاسم',
		'user_phone'			=> 'الهاتف',
		'user_email'			=> 'الايميل',
		'user_pass'				=> 'كلمة السر',
		'user_repeat_pass'		=> 'تأكيد كلمة السر',


		//send sms
		'sms_header'			=> 'SMS',
		'sms_remaining'			=> 'المتبقى',
		'sms_phone'				=> 'الهاتف',
		'sms_msg'				=> 'نص الرسالة',
		'send_msg'				=> 'رسالة SMS',		
		'send_success_msg' 		=> 'تم ارسال الرسالة بنجاح',
		'send_no_balance_msg' 	=> 'عفوا رصيدك لا يسمح',
		'send_error_msg' 		=> 'تأكد من رسالتك وأعد المحاولة',
		'send_btn' 				=> 'ارسال',

		//changes message
		'edit_success_msg'		=> 'تم حفظ التعديلات',
		'del_success_msg'		=> 'تم حذف العنصر بنجاح',	

];