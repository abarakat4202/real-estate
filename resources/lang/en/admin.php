<?php

return [

		//navbar and sidebar
		'dashboard' 			=> 'Dashboard',
		'welcome' 				=> 'Welcome',
		'settings' 				=> 'Settings',
		'global_settings' 		=> 'Global Settings',
		'slider' 				=> 'Slider',
		'most_popular' 			=> 'Most Popular',
		'about' 				=> 'About Page',
		'units'					=> 'Units',
		'add_unit'				=> 'Add Unit',
		'all_unit'				=> 'All Units',
		'users'					=> 'Users',
		'add_user'				=> 'Add User',
		'all_users'				=> 'All Users',
		'services'				=> 'Services',
		'add_service'			=> 'Add Service',
		'all_services'			=> 'All Services',
		'inquires'				=> 'Inquires',
		'sms_send'				=> 'Send_sms',		
		'new_messages'			=> 'New Messages',
		'view_messages'			=> 'View all messages',
		'new_alerts'			=> 'New Alerts',
		'view_alerts'			=> 'View all alerts',
		'logout_btn'			=> 'Logout',
		'msg_heading'			=> 'Message',
		'about_header'			=> 'About',

		//logout modal
		'logout_header'			=> 'Ready to Leave?',
		'logout_msg'			=> 'Select "Logout" below if you are ready to end your current session.',
		'cancel'				=> 'Cancel',

		//comomn varaibles
		'en_title'				=> 'Title (EN)',
		'ar_title'				=> 'Title (AR)',
		'en_des'				=> 'Description (EN)',
		'ar_des'				=> 'Description (AR)',
		'image'					=> 'Image',
		'save_btn'				=> 'Save',
		'submit_btn'			=> 'Submit',
		'date_lbl'				=> 'Date',
		'service_lbl'			=> 'Service',
		'unit_lbl'				=> 'Unit',
		'user_lbl'				=> 'User Name',
		'added_by_lbl'			=> 'Added By',
		'actions_lbl'			=> 'Actions',
		'name_lbl'				=> 'Name',
		'phone_lbl'				=> 'Phone',
		'email_lbl'				=> 'Email',
		'message_lbl'			=> 'Message',
		'actions_lbl'			=> 'Actions',
		'edit_btn'				=> 'Edit',
		'delete_btn'			=> 'Delete',
		'show_btn'				=> 'Show',
		'search_statement'		=> 'search for ...',

		//global settings 
		'global_header'			=> 'Global Settings',
		'website_ar_title'		=> 'Arabic Title',
		'website_en_title'		=> 'English Title',
		'website_phone'			=> 'Phone',
		'website_ar_address'	=> 'Arabic Address',
		'website_en_address'	=> 'English Address',
		'website_logo'			=> 'Website logo',
		'fb_page'				=> 'Facebook page',
		'slogan_en_title'		=> 'Slogan title (EN)',
		'slogan_ar_title'		=> 'Slogan title (AR)',
		'slogan_ar_des'			=> 'Slogan arabic description',
		'slogan_en_des'			=> 'Slogan english description',
		'slogan_img'			=> 'Slogan image',
		'partner_1'				=> 'Partner 1',
		'partner_2'				=> 'Partner 2',
		'partner_3'				=> 'Partner 3',
		'partner_4'				=> 'Partner 4',
		'partner_5'				=> 'Partner 5',

		//sliders
		'slider_header'			=> 'Slider',
		'slider1'				=> 'slider 1',
		'slider2'				=> 'slider 2',
		'slider3'				=> 'slider 3',

		//delete modal
		'del_header'			=> 'Delete this entry',
		'del_msg'				=> 'Are you sure you want to delete this record',
		'cancel'				=> 'Cancel',
		'confirm'				=> 'Yes',
		'not_confirm'			=> 'No',

		//message modal
		'message_heading'		=> 'Message',

		//login page
		'login_header'			=> 'Login',
		'login_id_lbl'			=> 'Email OR Phone',
		'login_pass_lbl'		=> 'Password',
		'remember_lbl'			=> 'Remember Password',
		'login_btn'				=> 'Login',

		//most_popular
		'unit_1'				=> 'Unit 1',
		'unit_2'				=> 'Unit 2',
		'unit_3'				=> 'Unit 3',

		//service
		'icon'					=> 'Icon',
		'service_header'		=> 'Service',

		//add unit
		'unit_header'			=> 'Unit',
		'unit_title'			=> 'Title',
		'unit_des'				=> 'Description',
		'unit_price'			=> 'Price',
		'unit_size'				=> 'Size',
		'unit_type'				=> 'Type',
		'unit_section'			=> 'Section',
		'unit_rooms'			=> 'Rooms',
		'unit_bathrooms'		=> 'Bathrooms',
		'unit_floor'			=> 'Floor - Floors',
		'unit_view'				=> 'View',
		'unit_build_year'		=> 'build year',
		'unit_delivery_date'	=> 'deliver date',
		'unit_pay_type'			=> 'Payment',
		'unit_loaction'			=> 'Location',
		'unit_address'			=> 'Address',

		//add user
		'user_header'			=> 'USER',
		'user_name'				=> 'Name',
		'user_phone'			=> 'Phone',
		'user_email'			=> 'Email',
		'user_pass'				=> 'Password',
		'user_repeat_pass'		=> 'Repeat password',


		//send sms
		'sms_header'			=> 'SMS',
		'sms_remaining'			=> 'Remaining',
		'sms_phone'				=> 'phone',
		'sms_msg'				=> 'message',
		'send_msg'				=> 'Send Message',		
		'send_success_msg' 		=> 'message has been send',
		'send_no_balance_msg' 	=> 'Sorry Insufficient Credit',
		'send_error_msg' 		=> 'Please make sure that your message is valid then try again',		
		'send_btn' 				=> 'Send',

		//changes message
		'edit_success_msg'		=> 'All changes has been saved',
		'del_success_msg'		=> 'The record has been deleted',	

];