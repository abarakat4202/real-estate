<?php
	return [


		//home page
		'home_btn'		=> 'Home',	
		'search_btn'	=> 'Search',	
		'select_location'=> 'Location',		
		'price_from'	=> 'price from',		
		'price_to'		=> 'price to',
		'all_units_btn'	=> 'All Units',								
		'units' 		=> 'Units',
		'services' 		=> 'Services',
		'about' 		=> 'About Us',
		'contact'		=> 'Contact Us',
		'most_popular' 	=> 'Most Popular',
		'partners' 		=> 'Our Partners',


		//all units
		'all_units_title' 	=> 'Recent Units',
		'recent_units' 		=> 'Recent Units',
		'size' 				=> 'Size in meters',
		'price' 			=> 'price',
		'read_more'     	=> 'More',
		'no_units_found'	=> 'no units found',

		//show unit
		'unit_latest_units' 	=> 'Latest Units',
		'unit_description'		=> 'Description',
		'unit_related_title'	=> 'Related Units',
		'read_more_unit'    	=> 'More',
		'unit_type'				=> 'Ad Type',
		'unit_section'			=> 'Section',
		'unit_location'			=> 'Location',
		'unit_floor'			=> 'Floor - Floors',
		'unit_view'				=> 'View',
		'unit_rooms'			=> 'Rooms',
		'unit_bathrooms'		=> 'Bathrooms',
		'unit_price'			=> 'Price',
		'unit_pay_type'			=> 'Pay Type',
		'unit_build_year'		=> 'Build Year',
		'unit_delivery_date'	=> 'Delivery Date',
		'unit_phone'			=> 'Phone',
		'unit_address'			=> 'Address',
		'contact_btn'			=> 'Contact Request',
		'unit_contact_head' 	=> 'Contact Request',
		'unit_contact_name' 	=> 'Name',
		'unit_contact_phone' 	=> 'Phone',
		'unit_contact_email' 	=> 'Email',
		'unit_contact_message' 	=> 'Message',
		'unit_contact_send' 	=> 'Send',
		'unit_applied_msg' 		=> 'you will be contacted shortly',
		'address'				=> 'Address',
		'phone'					=> 'Phone',
		'send_msg'				=> 'Send Message',

		//contact page
		'contact_h1'			=> 'Contact us',
		'contact_address_h1'	=> 'Address',
		'contact_phone_h1'		=> 'Phone',
		'contact_name_pl'		=> 'Name',
		'contact_phone_pl'		=> 'Phone',
		'contact_email_pl'		=> 'Email',
		'contact_message_pl'	=> 'Message',		
		'contact_submit_Btn'	=> 'Submit',

	];
