<?php 

	return [

		//unit variables
		'unit_type'		=> array(

			0 => 'For Sale',
			1 => 'For Rent',
			2 => 'For Sale or Rent'
		),

		'unit_view'			=> array(

			0 => 'Main Street',
			1 => 'Side Street',
			2 => 'Garden',
			3 => 'Corner',
			4 => 'Nile',
			5 => 'Sea',
			6 => 'naval',
		),

		'unit_pay_type'			=> array(

			0 => 'Cash',
			1 => 'Installment',
			2 => 'Cash or Installment',
			3 => '50% Advance Payment'			
		),

		'unit_applied_msg' => 'you will be contacted shortly',



	];
