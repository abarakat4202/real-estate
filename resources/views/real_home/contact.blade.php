@extends('real_home.app')
@section('title')

@endsection('title')

@section('content')
<!--contact-->
<div class="contact">
	<div class="container">
		<h3>{{trans('real_home.contact_h1')}}</h3>
	 <div class="contact-top">
		<div class="col-md-6 contact-top1">
		  <div class="contact-address">
		  	<div class="col-md-6 contact-address1">
			  	 <h5>{{trans('real_home.contact_address_h1')}}</h5>
	             <p><b>{{$settings[app('lang').'_title']['value']}}</b></p>
	             <p>{{$settings[app('lang').'_address']['value']}}</p>
		  	</div>
		  	<div class="col-md-6 contact-address1">
			  	 <h5 >{{trans('real_home.contact_phone_h1')}}</h5>
	             <p>{{$settings['phone']['value']}}</p>

	        </div>
		  	<div class="clearfix"></div>
		  </div>

		<div class="contact-address">
		  	<div class="col-md-6 contact-address1">
			  	 <h5>Facebook </h5>
	             <p><a href="{{$settings['fb']['value']}}" target="_blank">facebook</a></p>
	        </div>
		  	<div class="clearfix"></div>
		  </div>		  
		</div>
		<div class="col-md-6 contact-right">
	
            <form method="post" >
            	{{ csrf_field() }}
               <input type="text"  name="name" placeholder="{{trans('real_home.contact_name_pl')}}" required="">
               <input type="text"  name="phone" placeholder="{{trans('real_home.contact_phone_pl')}}" required="">
			   <input type="text"  name="email" placeholder="{{trans('real_home.contact_email_pl')}}" >
			   <textarea  placeholder="{{trans('real_home.contact_message_pl')}}" name="message" requried=""></textarea>
			   <label class="hvr-sweep-to-right">
	           <input type="submit" value="{{trans('real_home.contact_submit_Btn')}}">
	           </label>
			</form>
		</div>
		<div class="clearfix"> </div>
</div>
	</div>
</div>
<!--//contact-->

@endsection('content')