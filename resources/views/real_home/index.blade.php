@extends('real_home.app')
@section('title')
{{ $settings[app('lang').'_title']['value'] }}
@endsection('title')


@section('css')
<style type="text/css">
	.buy-media>i,.buy-media>h6
	{
		color: #1f568e!important;
	}
	.bottom-head:hover .fa 
	{
	    color: #ff8500!important;
	}
	.bottom-head a:hover .buy-media h6 
	{
    	color: #ff8500!important;
	}		
</style>
@endsection('css')
@section('content')
<div class=" header-right">
	<div class=" banner">
		 <div class="slider">
		    <div class="callbacks_container">
		      <ul class="rslides" id="slider">		       
				 <li>
		          	 <div class="banner1" style="	background: url('{{asset('uploads/app/'.$slider1['img'])}}') no-repeat;">
		           		<div class="caption">
				          	<h3><span>{{$slider1[app('lang').'_title']}}</span></h3>
				          	<p>{{$slider1[app('lang').'_des']}}</p>
		          		</div>
		          	</div>
		         </li>
				 <li>
		          	 <div class="banner2" style="	background: url('{{asset('uploads/app/'.$slider2['img'])}}') no-repeat;">
		           		<div class="caption">
				          	<h3><span>{{$slider2[app('lang').'_title']}}</span></h3>
				          	<p>{{$slider2[app('lang').'_des']}}</p>
		          		</div>
		          	</div>
		         </li>
		         <li>
		          	 <div class="banner3" style="	background: url('{{asset('uploads/app/'.$slider3['img'])}}') no-repeat;">
		           		<div class="caption">
				          	<h3><span>{{$slider3[app('lang').'_title']}}</span></h3>
				          	<p>{{$slider3[app('lang').'_des']}}</p>
		          		</div>
		          	</div>
		         </li>		
		      </ul>
		  </div>
		</div>
	</div>
</div>
 
<!--header-bottom-->
<div class="banner-bottom-top">
		<div class="container">
		<div class="bottom-header">
			<div class="header-bottom">
				<div class=" bottom-head">
					<a href="{{route('all_units',app('lang'))}}">
						<div class="buy-media radius-top">
						<i class="fa fa-university"> </i>
						<h6>{{trans('real_home.units')}}</h6>
						</div>
					</a>
				</div>
				<div class="bottom-head">
					<a href="#services">
						<div class="buy-media radius-top">
						<i class="fa fa-snowflake-o"> </i>
						<h6>{{trans('real_home.services')}}</h6>
						</div>
					</a>
				</div>
				<div class=" bottom-head">
					<a href="{{ route('about',app('lang')) }}">
						<div class="buy-media radius-top">
						<i class="fa fa-info"> </i>
						<h6>{{trans('real_home.about')}}</h6>
						</div>
					</a>
				</div>
				<div class=" bottom-head">
					<a href="{{ route('contact',app('lang')) }}">
						<div class="buy-media radius-top">
						<i class="fa fa-envelope"> </i>
						<h6>{{trans('real_home.contact')}}</h6>
						</div>
					</a>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
</div>
</div>
		<!--//-->
			
<!--//header-bottom-->


<!--//header-->
<div class="search-aqar">
	<div class="container">
		<form method="GET" action="/{{app('lang')}}/units">
			
			<div class="row">
				<div class="col-md-3 select-block">
		            <select class="form-control location" id="level_0" name="location_0">

		            </select>
		        </div>
		        <div class="col-md-3 select-block" >
		            <select class="form-control location" id="level_1" name="location_1">

		            </select>
		        </div>
		        <div class="col-md-3 select-block">
		        	<input type="number" required="" class="form-control" name="price_from" placeholder="{{trans('real_home.price_from')}}">

		        </div>
		        <div class="col-md-3 select-block">
		        	<input type="number" required="" class="form-control" name="price_to" placeholder="{{trans('real_home.price_to')}}">

		        </div>
		        <div class="col-md-2">
		        	<button type="submit" class="btn btn-warning btn-block"><b>{{trans('real_home.search_btn')}}</b></button>
		        </div>
			</div>
		</form>
	</div>
</div>
<div class="content">
	<div class="content-grid">
		<div class="container">
			<h3>{{ trans('real_home.most_popular') }}</h3>
			@foreach($populars as $pop)
				
			@if(!empty($pop->unit)) 
				<div class="col-md-4 box_2">
					 <a href="{{ $pop->unit->url() }}" class="mask">
				 	   	<img class="img-responsive img-units zoom-img img-units" src="{{ $pop->unit->img_url(0) }}" alt="{{ $pop->unit->title }}">
				 	   	<span class="four">{{ $pop->unit->price }}</span>
				 	 </a>
					   <div class="most-1">
				 	   	<h5><a href="{{  $pop->unit->url() }}">{{ $pop->unit->title }}</a></h5>
				 	    	<p>{{ str_limit($pop->unit->description, $limit = 100, $end = '...') }}</p>
				 	   </div>
					
				</div>
			@endif	
			@endforeach
		 	<div class="clearfix"> </div>
			<div class="col-md-2 col-md-offset-5">
	 			<a style="margin-top: 40px;" class="btn btn-lg btn-block btn-warning" href="/{{app('lang')}}/units"><b>{{trans('real_home.all_units_btn')}}</b></a>
	 		</div>		 	
		</div>
	</div>
<!--service-->
	<div class="services">
		<div class="container">
			<div class="service-top">
				<h3 id="services">{{ trans('real_home.services') }}</h3>
				
			</div>
			
			@foreach( array_chunk( $services->get()->all() , 2 ) as $row )
			<div class="services-grid">

				@foreach($row as $service)
					<div class="col-md-6 service-top1">
						<div class=" ser-grid">	
							<span class="hi-icon hi-icon-archive {{ $service->icon }}"> </span>
						</div>
						<div  class="ser-top">
							<h4>{{ $service[app('lang').'_'.'title'] }}</h4>
							<p>{{ $service[app('lang').'_'.'des'] }}</p>
						</div>
						<div class="clearfix"> </div>
					</div>
				@endforeach
				<div class="clearfix"> </div>
			</div>
			@endforeach
		</div>
	</div>
<!--//services-->
<!--features-->
		<div class="content-middle" style="  
		background: url({{asset('/uploads/app/'.$slogan['img'])}}) no-repeat center;
		    width: 100%;
    min-height: 250px;
    display: block;
    background-size: cover;">
			<div class="container">
				<div class="mid-content">
					<h3>{{ $slogan[app('lang').'_title'] }}</h3>
					<p>{{ $slogan[app('lang').'_des'] }}</p>
				</div>
			</div>
		</div>
<!--//features-->
<!--phone-->
<!--//phone-->

<!--test-->
			
<!--//test-->	
<!--partners-->
	<div class="content-bottom1">
		<h3>{{ trans('real_home.partners') }}</h3>
	 		<div class="container">
				<ul>
					<li><a href="#"><img class="img-responsive" src="{{ asset('uploads/app/'. $partners_imgs['partner1']) }}" alt=""></a></li>
					<li><a href="#"><img class="img-responsive" src="{{ asset('uploads/app/'. $partners_imgs['partner2']) }}" alt=""></a></li>
					<li><a href="#"><img class="img-responsive" src="{{ asset('uploads/app/'. $partners_imgs['partner3']) }}" alt=""></a></li>
					<li><a href="#"><img class="img-responsive" src="{{ asset('uploads/app/'. $partners_imgs['partner4']) }}" alt=""></a></li>
					<li><a href="#"><img class="img-responsive" src="{{ asset('uploads/app/'. $partners_imgs['partner5']) }}" alt=""></a></li>
				<div class="clearfix"> </div>
				</ul>
			</div>
		</div>	
<!--//partners-->	
</div>
@endsection('content')

@section('js')
<script>
$(function(){
	//locations handle

	//get locations 
	var locations = {!! $locations !!};
	var level = [];

	//helper function to filter locations
	function filter_locations(vars){

		return vars[this[0]] == this[1];
	}

	//helper function to fill select
	function fill_locations(el,arr)
	{
		el.empty();
		el.append($('<option disabled="disabled" selected="selected" value="">{{trans('real_home.select_location')}}</option>'));
		for(var i in arr)
		{
			var location = arr[i];
			var opt = $('<option value="'+location["id"]+'">'+location["{{app('lang')}}_title"]+'</option>');

			el.append(opt);
		}
	}	


	//get main locations and fill it to select elment
	level[0] = locations.filter(filter_locations,['level',0]);
	level[1] = locations.filter(filter_locations,['parent_id',2]);
	fill_locations($('#level_0'),level[0]);
	fill_locations($('#level_1'),level[1]);
	$('#level_0').val(2);
	$('#level_1').val(32);

	$( ".location" ).each(function( index ) 
	{
		$(this).change(function()
		{
			var next_index = index+1;
			child_locations = locations.filter(filter_locations,['parent_id',$(this).val()]);
			child_locations = child_locations.filter(function(vars){return !!vars['parent_id']});
			var child_level = $(".location").eq(next_index);

			//check if there is child locations
			if(child_locations.length)
			{
				child_level.show().attr("required","required");
				fill_locations(child_level,child_locations);

			}
			else if( next_index < $(".location").length)
			{
				child_level.empty();
				child_level.hide().removeAttr("required");
				
			}
			//run change to run trigger to next
			child_level.change();

			//store the actual location id in the holder
			$('#location_holder').val($(this).val());
		});
	});
})
</script>
@endsection('js')
