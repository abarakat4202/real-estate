@extends('real_home.app')
@section('title')
{{trans('real_home.all_units_title')}}
@endsection('title')

@section('content')

<!--Dealers-->
<div class="dealers " >
	<div class="container">
		<div class="dealer-top">
			@if(count($units))
			<h4>{{trans('real_home.recent_units')}}</h4>
				@foreach(array_chunk($units->all(),4) as $row)
				<div class="deal-top-top ">
					@foreach($row as $unit)
					<div class="col-md-3 top-deal-top">
						<div class=" top-deal">
							<a href="{{ $unit->url() }}" class="mask"><img src="{{ $unit->img_url(0) }}" class="img-responsive zoom-img img-units" alt=""></a>
							<div class="deal-bottom">
								<div class="top-deal1">
									<h5><a href="{{ $unit->url() }}">{{ $unit->title }}</a></h5>
									<p>{{trans('real_home.size')}} : {{ $unit->size }}</p>
									<p>{{trans('real_home.price')}} : {{ $unit->price }}</p>
								</div>
								<div class="top-deal2">
									<a href="{{ $unit->url() }}" class="hvr-sweep-to-right more ">{{trans('real_home.read_more')}}</a>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
					</div>
					@endforeach
					<div class="clearfix"> </div>
				@endforeach
				</div>
				<div class="row text-center app_pagination" style="margin-top: 50px;">

					{{ $units->appends(Request::input())->render("pagination::bootstrap-4") }}	
				</div>
				<div class="clearfix"> </div>
			
			@else
			<h4>{{trans('real_home.no_units_found')}}</h4>
			@endif
		</div>
	</div>
</div>
@endsection('content')
