@extends('real_home.app')
@section('title')
{{ $unit->title .' | '.$settings[app('lang').'_title']['value']}}
@endsection('title')

@section('description')
{{ $unit->description}}
@endsection('description')

@section('h1')
{{ $unit->title }}
@endsection('h1')
@section('css')
<style type="text/css">
	.custom_btn
	{
		border-radius: 0px !important;
	    font-size: 18px !important;
        border: 1px solid #27da93;
		color: #27da93;
	    padding: 0.3em 0.7em;
    	background-color: #fff;
	}
	.custom_btn:hover
	{
		color:#fff;
	}
	.unit_label
	{
		min-width: 600px!important;
	}
</style>
@endsection('css')
@section('content')



<div class="container">

	

	<div class="buy-single-single">

	

		<div class="col-md-9 single-box">

	   		<div class=" buying-top">	

				<div class="flexslider">

					<ul class="slides">

						@foreach($unit->images() as $key => $img)

						<li data-thumb="{{ $unit->img_url($key) }}">

						  <img src="{{ $unit->img_url($key) }}" />

						</li>
						@endforeach

					</ul>

				</div>

				<!-- FlexSlider -->

				<script defer src="{{asset('real_home/js/jquery.flexslider.js')}}"></script>

				<link rel="stylesheet" href="{{asset('real_home/css/flexslider.css')}}" type="text/css" media="screen" />



				<script>

				// Can also be used with $(document).ready()

				$(window).load(function() {

				$('.flexslider').flexslider({

				animation: "slide",

				controlNav: "thumbnails"

				});

				});

				</script>


				<div class="buy-sin-single">

					<div class="col-sm-5 middle-side immediate">

						<h4>{{$unit -> title}}</h4>


						<!-- Ad Type -->
						<p><span class="bold ">{{trans('real_home.unit_type')}}</span> : <span class="two">{{ trans('app.unit_type')[$unit->type]}}</span></p>
			 			
		
						<!-- section -->
						@if(!empty($unit ->section))
						<p><span class="bold ">{{trans('real_home.unit_section')}}</span> : <span class="two">{{ $unit->section[app('lang').'_section']}}</span></p>
			 			@endif

			 			<!-- location -->
						<p><span class="bold ">{{trans('real_home.unit_location')}}</span> : <span class="two">{{ implode('/',$unit->location_tree()) }}</span></p>
			 
						<!-- floor -->
						@if(!empty($unit ->floor))
						<p><span class="bold ">{{trans('real_home.unit_floor')}}</span> : <span class="two">{{ $unit->floor }}</span></p>
			 			@endif		

						<!-- view -->
						
						<p><span class="bold ">{{trans('real_home.unit_view')}}</span> : <span class="two">{{ trans('app.unit_view')[$unit->view]}}</span></p>
			 					

						<!-- rooms -->
						@if(!empty($unit ->rooms))
						<p><span class="bold ">{{trans('real_home.unit_rooms')}}</span> : <span class="two">{{ $unit->rooms }}</span></p>
			 			@endif		

						<!-- boldrooms -->
						@if(!empty($unit ->boldrooms))
						<p><span class="bold ">{{trans('real_home.unit_boldrooms')}}</span> : <span class="two">{{ $unit->boldrooms }}</span></p>
			 			@endif		

						<!-- price -->
						@if(!empty($unit ->price))
						<p><span class="bold ">{{trans('real_home.unit_price')}}</span> : <span class="two">{{ $unit->price }}</span></p>
			 			@endif		

						<!-- pay_type -->
						<p><span class="bold ">{{trans('real_home.unit_pay_type')}}</span> : <span class="two">{{ trans('app.unit_pay_type')[$unit->pay_type] }}</span></p>


						<!-- build_year -->
						@if(!empty($unit ->build_year))
						<p><span class="bold ">{{trans('real_home.unit_build_year')}}</span> : <span class="two">{{ $unit->build_year }}</span></p>
			 			@endif

						<!-- delivery_date -->
						@if(!empty($unit ->delivery_date))
						<p><span class="bold ">{{trans('real_home.unit_delivery_date')}}</span> : <span class="two">{{ $unit->delivery_date }}</span></p>
			 			@endif	

			 			@auth
							<!-- phone -->
							@if(!empty($unit ->phone))
							<p><span class="bold ">{{trans('real_home.unit_phone')}}</span> : <span class="two">{{ $unit->phone }}</span></p>
				 			@endif	

							<!-- address -->
							@if(!empty($unit ->address))
							<p><span class="bold ">{{trans('real_home.unit_address')}}</span> : <span class="two">{{ $unit->address }}</span></p>
				 			@endif		
			 			@endauth

						<div class="   right-side">

		                    <button class="btn hvr-sweep-to-right more custom_btn" data-title="Delete" data-id="{{ $unit->id }}" data-toggle="modal" data-target="#delete" 
		                    	>{{trans('real_home.contact_btn')}}</button >						    
						</div>

					</div>

					<div class="col-sm-7 buy-sin">

						<h4>{{trans('real_home.unit_description')}}</h4>

						<p>{{ $unit->description}}</p>

					</div>

					<div class="clearfix"> </div>

				</div>

			</div>
		</div>
		<div class="col-md-3">

			<div class="single-box-right right-immediate">
				
				@if(!empty($related[0]))
		     	<h4>{{trans('real_home.unit_related_title')}}</h4>
		     	@endif
				
				<!--loop into related units -->
				@foreach($related as $r_unit)
				
				<div class="single-box-img ">

					<div class="box-img">

						<a href="{{ $r_unit->url() }}"><img class="img-responsive zoom-img img-units" src="{{ $r_unit->img_url(0) }}" alt=""></a>

					</div>

					<div class="box-text">

						<p><a href="{{ $r_unit->url() }}">{{ $r_unit->title }}</a></p>

						<a href="{{ $r_unit->url() }}" class="in-box">{{trans('real_home.read_more_unit')}}</a>

					</div>

					<div class="clearfix"> </div>

				</div>
				@endforeach
				<!-- end loop-->

			</div>
		</div>
		<div class="clearfix"> </div>

		

	</div>

</div>

<!---->

<div class="container">

	<div class="future">

		<h3 >{{trans('real_home.unit_latest_units')}}</h3>

		<div class="content-bottom-in">

			<ul id="flexiselDemo1">			
				<!--loop into Latest units -->
				@foreach($latest as $l_unit)
				<li>
					<div class="project-fur">

						<a href="{{ $l_unit->url() }}" ><img class="img-responsive" src="{{ $l_unit->img_url(0) }}" alt="" />	</a>

							<div class="fur">

								<div class="fur1">

                                    <span class="fur-money">{{ $l_unit->price }}</span>

                                    <h6 class="fur-name"><a href="{{ $l_unit->url() }}">{{ $l_unit->title }}</a></h6>

                                   	<span>{{ $l_unit->location[app('lang').'_title'] }}</span>

                       			</div>

	                            <div class="fur2">

	                               	<span>{{ $l_unit->location[app('lang').'_title'] }}</span>

	                             </div>

							</div>					

					</div>
				</li>
				<!--end loop-->
				@endforeach


			</ul>

			<script type="text/javascript">

				$(window).load(function() {

					$("#flexiselDemo1").flexisel({

						visibleItems: 4,

						animationSpeed: 1000,

						autoPlay: true,

						autoPlaySpeed: 3000,    		

						pauseOnHover: true,

						enableResponsiveBreakpoints: true,

				    	responsiveBreakpoints: { 

				    		portrait: { 

				    			changePoint:480,

				    			visibleItems: 1

				    		}, 

				    		landscape: { 

				    			changePoint:640,

				    			visibleItems: 2

				    		},

				    		tablet: { 

				    			changePoint:768,

				    			visibleItems: 3

				    		}

				    	}

				    });

				    

				});

			</script>

			<script type="text/javascript" src="{{ asset('real_home/js/jquery.flexisel.js') }}"></script>

		</div>


	</div>

	

</div>

<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
        <h4 class="modal-title custom_align" id="Heading">{{trans('real_home.unit_contact_head')}}</h4>
      </div>
	  <form method="POST">
      <div class="modal-body">
		
		
			{{ csrf_field() }}
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>{{trans('real_home.unit_contact_name')}} :</label>
						<input type="Text" class="form-control" name="name" required="">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>{{trans('real_home.unit_contact_phone')}} :</label>
						<input type="number" class="form-control" name="phone" required="">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>{{trans('real_home.unit_contact_email')}} :</label>
						<input type="email" class="form-control" name="email">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>{{trans('real_home.unit_contact_message')}} :</label>
						<textarea name="message" class="form-control" rows="10"></textarea>
					</div>
				</div>
			</div>

		
      </div>
      <div class="modal-footer text-center">

        <button class="btn hvr-sweep-to-right col-md-4 col-md-offset-4 custom_btn" href="" id="modal_action" type="submit" >{{trans('real_home.unit_contact_send')}}</button>

      </div>
      </form>

    </div>
  </div>
</div>
@endsection('content')
@section('js')
<script src="{{asset('real_home/js/bootstrap.min.js')}}"></script>
@endsection('js')
