@if(Session::has('messages'))
<div class="container" style="margin-top: 20px">
	<div class="form-group">
		<div class="alert alert-success">
			<ul>
				@foreach(Session::get('messages') as $msg )
					<li>{{$msg}}</li>
				@endforeach
			</ul>
		</div>

	</div>	
</div>

@endif