

<!DOCTYPE html>
<html>
<head>
<title>@yield('title')</title>
<link href="{{asset('real_home/css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all" />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{ asset('real_home/js/jquery.min.js') }}"></script>
<!-- Custom Theme files -->
<!--menu-->
<script src="{{ asset('real_home/js/scripts.js')}}"></script>
<link href="{{ asset('real_home/css/styles.css') }}" rel="stylesheet">
<!--//menu-->
<!-- Font Awesome -->
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

@if(app('lang') == 'ar')
	<!--rtl bootstrap -->
	<link rel="stylesheet" href="{{asset('real_home/css/bootstrap-rtl.min.css')}}">
@endif
<!--theme-style-->
<link href="{{ asset('real_home/css/style.css') }}" rel="stylesheet" type="text/css" media="all" />
<link href="{{ asset('real_home/css/custom.css') }}" rel="stylesheet" type="text/css" media="all" />
<!-- Fonts --> <!-- Update 2/2/2018 -->
<link href="https://fonts.googleapis.com/css?family=Cairo" rel="stylesheet">
@if(app('lang') == 'ar')
	<!--rtl bootstrap -->
	<link rel="stylesheet" href="{{asset('real_home/css/arcustom.css')}}">
@endif
@yield('css')	
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- slide -->
<script src="{{ asset('real_home/js/responsiveslides.min.js')}}"></script>
   <script>
    $(function () {
      $("#slider").responsiveSlides({
      	auto: true,
      	speed: 500,
        namespace: "callbacks",
        pager: true,
      });
    });
  </script>
</head>
<body >
<!--header-->

<div class="header" style="    background-color: #dff0d8;
    padding: 2px 0;">
	<div class="container">

		<!--logo-->
		<div class="logo">
			<h1><a href="{{ route('home',app('lang')) }}"><img style="height: 100px;
    width: 161.8px;" class="img-responsive main-logo" src="{{asset('uploads/app/'.$settings['logo_img']['value'])}}"></a></h1>
		</div>
		<!--//logo-->
		<div class="top-nav" style="
    font-size:  25px;
    margin-top:  20px;
">
			<div class="right-icons">
				<ul class="right-icons">
					<li ><span style="
    color: #1f568e;
"><i class="glyphicon glyphicon-phone"> </i><a href="tel:{{ $settings['phone']['value'] or ''  }}">{{ $settings['phone']['value'] or ''  }}</a> </span></li>
					<li>
						<a class="btn  btn btn-warning" href="/lang/@if(app('lang')=='ar'){{'en'}}@else{{'ar'}}@endif">
						@if(app('lang')=='ar'){{'English'}}@else{{'عربى'}}@endif
						</a>
					</li>

				</ul>
			</div>

			<div class="nav-icon">
				<!--
				<a href="#" class="right_bt" id="activator"><i class="glyphicon glyphicon-menu-hamburger"></i>  </a> -->
			
			</div>
			<div class="clearfix"> </div>
			<!---pop-up-box---->
			   
				<link href="{{ asset('real_home/css/popuo-box.css') }}" rel="stylesheet" type="text/css" media="all"/>
				<script src="{{ asset('real_home/js/jquery.magnific-popup.js') }}" type="text/javascript"></script>
			<!---//pop-up-box---->
				<div id="small-dialog" class="mfp-hide">
					    <!----- tabs-box ---->
				<div class="sap_tabs">	
				     <div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
						  <ul class="resp-tabs-list">
						  	  <li class="resp-tab-item " aria-controls="tab_item-0" role="tab"><span>All Homes</span></li>
							  <li class="resp-tab-item" aria-controls="tab_item-1" role="tab"><span>For Sale</span></li>
							  <li class="resp-tab-item" aria-controls="tab_item-2" role="tab"><span>For Rent</span></li>
							  <div class="clearfix"></div>
						  </ul>				  	 
						  <div class="resp-tabs-container">
						  		<h2 class="resp-accordion resp-tab-active" role="tab" aria-controls="tab_item-0"><span class="resp-arrow"></span>All Homes</h2><div class="tab-1 resp-tab-content resp-tab-content-active" aria-labelledby="tab_item-0" style="display:block">
								 	<div class="facts">
									  	<div class="login">
											<input type="text" value="Search Address, Neighborhood, City or Zip" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search Address, Neighborhood, City or Zip';}">		
									 		<input type="submit" value="">
									 	</div>        
							        </div>
						  		</div>
							     <h2 class="resp-accordion" role="tab" aria-controls="tab_item-1"><span class="resp-arrow"></span>For Sale</h2><div class="tab-1 resp-tab-content" aria-labelledby="tab_item-1">
									<div class="facts">									
										<div class="login">
											<input type="text" value="Search Address, Neighborhood, City or Zip" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search Address, Neighborhood, City or Zip';}">		
									 		<input type="submit" value="">
									 	</div> 
							        </div>	
								 </div>									
							      <h2 class="resp-accordion" role="tab" aria-controls="tab_item-2"><span class="resp-arrow"></span>For Rent</h2><div class="tab-1 resp-tab-content" aria-labelledby="tab_item-2">
									 <div class="facts">
										<div class="login">
											<input type="text" value="Search Address, Neighborhood, City or Zip" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search Address, Neighborhood, City or Zip';}">		
									 		<input type="submit" value="">
									 	</div> 
							         </div>	
							    </div>
					      </div>
					 </div>
					 <script src="{{asset('real_home/js/easyResponsiveTabs.js')}}" type="text/javascript"></script>
				    	<script type="text/javascript">
						    $(document).ready(function () {
						        $('#horizontalTab').easyResponsiveTabs({
						            type: 'default', //Types: default, vertical, accordion           
						            width: 'auto', //auto or any width like 600px
						            fit: true   // 100% fit in a container
						        });
						    });
			  			 </script>	
				</div>
				</div>
				 <script>
						$(document).ready(function() {
						$('.popup-with-zoom-anim').magnificPopup({
							type: 'inline',
							fixedContentPos: false,
							fixedBgPos: true,
							overflowY: 'auto',
							closeBtnInside: true,
							preloader: false,
							midClick: true,
							removalDelay: 300,
							mainClass: 'my-mfp-zoom-in'
						});
																						
						});
				</script>
					
	
		</div>
		<div class="clearfix"> </div>
		</div>	
</div>
<!--//-->	

<!--content-->
<!--//-->	
@if(empty($home) )
<div class=" banner-buying">

	<div class=" container">
    
		<h3>@yield('h1')</h3> 

		<!---->

		<div class="menu-right">

		</div>

		<div class="clearfix"> </div>

	</div>

</div>
@endif

<!--//header-->

<!---->
@include('real_home.messages')
	    <link rel="stylesheet" href="{{asset('real_home/css/social-share-kit.css?v=1.0.14')}}">
        

        <div class="ssk-sticky ssk-left ssk-center ssk-lg">
                <a href="https://www.facebook.com/dialog/share?app_id=140586622674265&display=popup&href={{Request::url()}}#.WYPZLRwJSiU.facebook&picture=&title=@yield('title')&description=&redirect_uri=http://s7.addthis.com/static/thankyou.html" class="ssk ssk-facebook" data-ssk-ready="true" target="_blank"></a>
                <a href="https://twitter.com/intent/tweet?url={{Request::url()}}" class="ssk ssk-twitter" data-ssk-ready="true" target="_blank"></a>
                <a href="https://plus.google.com/share?url={{Request::url()}}" class="ssk ssk-google-plus" data-ssk-ready="true" target="_blank"></a>
                <a href="https://www.linkedin.com/shareArticle?url={{Request::url()}}" class="ssk ssk-linkedin" data-ssk-ready="true" target="_blank"></a>
                <a href="mailto:?subject={{Request::url()}}&amp;body={{Request::url()}}"
                 class="ssk ssk-email" data-ssk-ready="true"></a>
        </div>
@yield('content')

<!--footer-->
<div class="footer">
	<div class="container">
		<div class="col-md-6 footer-top-at">
			<div class="">
				<ul class="nav-bottom">
					<li><p>{{trans('real_home.address')}}: {{ $settings[app('lang').'_address']['value'] or ''  }}</p></li>
					<li><p>{{trans('real_home.phone')}}: <a href="tel:{{ $settings['phone']['value'] or ''  }}">{{ $settings['phone']['value'] or ''  }}</a> </p></li>
					<li><a href="{{route('contact',app('lang'))}}">{{trans('real_home.send_msg')}}</a></li>
					<li><a href="{{$settings['fb']['value']}}"><i class="fa fa-facebook-f"></i> Facebook</a> </li>
				</ul>	
			</div>
		<div class="clearfix"> </div>
		</div>
		<div class="col-md-6 footer-class footer-right">
				<p >© {{ \Carbon\Carbon::now()->year }} {{$settings[app('lang').'_title']['value']}}. All Rights Reserved | Developed by  <a href="http://zero1business.com/" target="_blank">zero1business</a> </p>
		</div>
	</div>
</div>
<!--//footer-->
@yield('js')
<script src="{{asset('real_home/js/social-share-kit.min.js')}}"></script> 
</body>
</html>