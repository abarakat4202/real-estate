@extends('real_home.app')
@section('title')

@endsection('title')
@section('content')
<div class="about">	
	<div class="about-head">
		<div class="container">
			<h3>About Us</h3>
				<div class="about-in">
					<img src="{{asset('/uploads/app/'.$arr['img'])}}" alt="image" class="img-responsive " style="display: none;">			
					<h6 ><span >{{ $arr[app('lang').'_title'] }}</span></h6>
					<p>{{ $arr[app('lang').'_des'] }}</p>
				</div>
		</div>
	</div>
</div>


@endsection('content')