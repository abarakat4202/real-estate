@extends('admin.layouts.app')
@section('content')
<div class="row">
	<div class="col-md-12">
	<div class="slider">
	<div class="item-collapse">
	<div class="card card-register mx-auto mt-5">
	<div class="card-header">{{trans('admin.slider_header')}}</div>
	<div class="card-body">

		<form method="POST" enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="panel-group" id="accordion">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
						  <a data-toggle="collapse" data-parent="#accordion" href="#slider1">
						  {{trans('admin.slider1')}}</a>
						</h4>
					</div>
					<div id="slider1" class="panel-collapse collapse in">
						<div class="panel-body">	       
						    <div class="form-group">
						      <div class="form-row">
						        <div class="col-md-6">
						          <label for="exampleInputName">{{trans('admin.en_title')}}: </label>
						          <input class="form-control" id="title-en" name="slider1_en_title" type="text" aria-describedby="nameHelp" placeholder="{{trans('admin.en_title')}}" value="{{$slider1['en_title']->value or ''}}">
						        </div>
						        <div class="col-md-6">
						          <label for="exampleInputLastName">{{trans('admin.ar_title')}}: </label>
						          <input class="form-control" id="title-ar" name="slider1_ar_title" type="text" aria-describedby="nameHelp" placeholder="{{trans('admin.ar_title')}}" value="{{$slider1['ar_title']->value or ''}}">
						        </div>
						      </div>
						    </div>
						    <div class="form-group">
						      <div class="form-row">
						        <div class="col-md-6">
						          <label for="exampleInputName">{{trans('admin.en_des')}}: </label>
						          <textarea class="form-control" id="desc-en" name="slider1_en_des" type="text" aria-describedby="nameHelp" placeholder="{{trans('admin.en_des')}}">{{$slider1['en_des']->value or ''}}</textarea>
						        </div>
						        <div class="col-md-6">
						          <label for="exampleInputLastName">{{trans('admin.ar_des')}}: </label>
						          <textarea class="form-control" id="desc-en" name="slider1_ar_des" type="text" aria-describedby="nameHelp" placeholder="{{trans('admin.ar_des')}}">
						          {{$slider1['ar_des']->value or ''}}
						          </textarea>
						        </div>
						      </div>
						    </div>
						    <div class="form-group">
		                        <div class="col-md-12">
		                          <label for="">{{trans('admin.image')}}: </label>
		                          <input type='file' id="imgInp" name="slider1_img" class="upload"/>
		                          <div id='img_contain'><img id="blah" align='middle' alt="your image" title=' upload a new image' src="{{asset('uploads/app/').'/'.$slider1['img']->value}}"/>
		                          </div>
		                        </div>

						    </div>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
						  <a data-toggle="collapse" data-parent="#accordion" href="#slider2">
						  {{trans('admin.slider2')}}</a>
						</h4>
					</div>
					<div id="slider2" class="panel-collapse collapse in">
						<div class="panel-body">	       
						    <div class="form-group">
						      <div class="form-row">
						        <div class="col-md-6">
						          <label for="exampleInputName">{{trans('admin.en_title')}}: </label>
						          <input class="form-control" id="title-en" name="slider2_en_title" type="text" aria-describedby="nameHelp" placeholder="{{trans('admin.en_title')}}" value="{{$slider2['en_title']->value or ''}}">
						        </div>
						        <div class="col-md-6">
						          <label for="exampleInputLastName">{{trans('admin.ar_title')}}: </label>
						          <input class="form-control" id="title-ar" name="slider2_ar_title" type="text" aria-describedby="nameHelp" placeholder="{{trans('admin.ar_title')}}" value="{{$slider2['ar_title']->value or ''}}">
						        </div>
						      </div>
						    </div>
						    <div class="form-group">
						      <div class="form-row">
						        <div class="col-md-6">
						          <label for="exampleInputName">{{trans('admin.en_des')}}: </label>
						          <textarea class="form-control" id="desc-en" name="slider2_en_des" type="text" aria-describedby="nameHelp" placeholder="{{trans('admin.en_des')}}" >
						          	{{$slider2['en_des']->value or ''}}
						          </textarea>
						        </div>
						        <div class="col-md-6">
						          <label for="exampleInputLastName">{{trans('admin.ar_des')}}: </label>
						          <textarea class="form-control" id="desc-en" name="slider2_ar_des" type="text" aria-describedby="nameHelp" placeholder="{{trans('admin.ar_des')}}">
						          	{{$slider2['ar_des']->value or ''}}
						          </textarea>
						        </div>
						      </div>
						    </div>
						    <div class="form-group">
		                        <div class="col-md-12">
		                          <label for="">{{trans('admin.image')}}: </label>
		                          <input type='file' id="imgInp" name="slider2_img" class="upload"/>
		                          <div id='img_contain'><img id="blah" align='middle' src="{{asset('uploads/app/').'/'.$slider2['img']->value}}" alt="your image" title=' upload a new image'/>
		                          </div>
		                        </div>

						    </div>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
						  <a data-toggle="collapse" data-parent="#accordion" href="#slider3">{{trans('admin.slider3')}}</a>
						</h4>
					</div>
					<div id="slider3" class="panel-collapse collapse in">
						<div class="panel-body">	       
						    <div class="form-group">
						      <div class="form-row">
						        <div class="col-md-6">
						          <label for="exampleInputName">{{trans('admin.en_title')}}: </label>
						          <input class="form-control" id="title-en" name="slider3_en_title" type="text" aria-describedby="nameHelp" placeholder="{{trans('admin.en_title')}}" value="{{$slider3['en_title']->value or ''}}">
						        </div>
						        <div class="col-md-6">
						          <label for="exampleInputLastName">{{trans('admin.ar_title')}}: </label>
						          <input class="form-control" id="title-ar" name="slider3_ar_title" type="text" aria-describedby="nameHelp" placeholder="{{trans('admin.en_title')}}" value="{{$slider3['ar_title']->value or ''}}">
						        </div>
						      </div>
						    </div>
						    <div class="form-group">
						      <div class="form-row">
						        <div class="col-md-6">
						          <label for="exampleInputName">{{trans('admin.en_des')}}: </label>
						          <textarea class="form-control" id="desc-en" name="slider3_en_des" type="text" aria-describedby="nameHelp" placeholder="{{trans('admin.en_des')}}">
						          	{{$slider3['en_des']->value or ''}}
						          </textarea>
						        </div>
						        <div class="col-md-6">
						          <label for="exampleInputLastName">{{trans('admin.ar_title')}}: </label>
						          <textarea class="form-control" id="desc-en" name="slider3_ar_des" type="text" aria-describedby="nameHelp" placeholder="{{trans('admin.ar_title')}}">
						          	{{$slider3['ar_des']->value or ''}}
						          </textarea>
						        </div>
						      </div>
						    </div>
						    <div class="form-group">
		                        <div class="col-md-12">
		                          <label for="">{{trans('admin.image')}}: </label>
		                          <input type='file' id="imgInp" name="slider3_img" class="upload"/>
		                          <div id='img_contain'><img id="blah" align='middle' src="{{asset('uploads/app/').'/'.$slider3['img']->value}}" alt="your image" title=' upload a new image'/>
		                          </div>
		                        </div>

						    </div>
						</div>
					</div>
				</div>
			</div>

            <div class="form-group">
          		<input class="btn btn-success btn-block" type="submit" value="{{trans('admin.save_btn')}}">
            </div>
		</form>
	</div>
	</div>
	</div>
	</div>
	</div>
</div>
@endsection('content')
@section('js')
<script type="text/javascript">
function readURL(el,input) {

  if (input.files && input.files[0]) 
  {
    var reader = new FileReader();

    reader.onload = function(e) {
      el.next().find('img').attr('src', e.target.result);

      el.next().find('img').hide();
      el.next().find('img').fadeIn(650);

    }

    reader.readAsDataURL(input.files[0]);
  }
}

$(".upload").change(function() {
  readURL($(this),this);
});
$('form').submit(function() 
{
        //send null values
        $(this).find(":input").filter(function(){return !this.value;}).value(null);
        $(this).find("textarea").filter(function(){return !this.value;}).value(null);

        $('form').find('input[type="file"]').filter(function(){return !this.value;}).value('');

});
</script>
@endsection('js')