@extends('admin.layouts.app')
@section('content')
<div class="row">

  <div class="col-md-12">
    <form method="get">

      <div class="row">
          <div class="col-md-8">
            <div class="input-group">
              <input class="form-control" name="query" value="{{ Request('query') }}" type="text" placeholder="{{trans('admin.search_statement')}}">
              <span class="input-group-append">
                <button class="btn btn-primary" type="submit">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div>
          </div>
          <div class="col-md-4 " hidden>
            <div class="dropdown">
              <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Sort
              <span class="caret"></span></button>
              <ul class="dropdown-menu">
                <li class="li"><a href="#">Ascending</a></li>
                <li class="li"><a href="#">Descending</a></li>
              </ul>
            </div>
          </div>
        
      </div>
    </form>
    <div class="table-responsive">    
          <table id="mytable" class="table table-bordred table-striped"> 
             <thead>
                <th>{{trans('admin.date_lbl')}}</th>
                <th>{{trans('admin.name_lbl')}}</th>
                <th>{{trans('admin.phone_lbl')}}</th>
                <th>{{trans('admin.email_lbl')}}</th>
                <th>{{trans('admin.unit_lbl')}}</th>
                <th>{{trans('admin.message_lbl')}}</th>

             </thead>
             <tbody>
              @foreach($inquiries as $inquiry )
                <tr>
                  <td>{{ $inquiry->created_at }}</td>
                  <td>{{ $inquiry->name}}</td>
                  <td>{{ $inquiry->phone }}</td>
                  <td>{{ $inquiry->email }}</td>
                  <td>
                  @if(!empty($inquiry->unit))
                    <a href="{{ $inquiry->unit->url() }}" target="_blank">{{ $inquiry->unit->title or '' }}</a>
                  @endif
                  </td>
                  <td class="center" >
                    <button class="btn btn-primary show" data-title="show" data-id="{{ $inquiry->message }}" data-toggle="modal" data-target="#show">{{ trans('admin.show_btn') }}</button>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        <div class="clearfix"></div>
    </div>
  </div>
</div>
{{ $inquiries->appends(Request::input())->render("pagination::bootstrap-4") }} 
<div class="modal fade" id="show" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
        <h4 class="modal-title custom_align" id="Heading">{{ trans('admin.message_heading') }}</h4>
      </div>

      <div class="modal-body">
        
      </div>

    </div>
  </div>
</div>
@endsection('content')
@section('js')
<script type="text/javascript">
$(function(){
  'use strict';

  //create trigger to add id of item to modal box
  $('.show').click(function(){


    $('.modal-body').html($(this).attr('data-id'));
  });
});
</script>
@endsection('js')
