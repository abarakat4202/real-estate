@extends('admin.layouts.app')
@section('content')
<div class="row">
	<div class="col-md-12">
	  	<div class="add-service">
	    
	  		<div class="card card-register mx-auto mt-5">
	            <div class="card-header">{{trans('admin.service_header')}}</div>
	            <form method="POST">
	            	{{ csrf_field() }}
	              	<div class="card-body">
	                
						<div class="form-group">
						<div class="form-row">
						  <div class="col-md-6">
						    <label for="exampleInputName">{{ trans('admin.en_title') }}: </label>
						    <input class="form-control" id="title-en" name="en_title" type="text" aria-describedby="nameHelp" placeholder="{{ trans('admin.en_title') }}" value="{{ $service->en_title or '' }}">
						  </div>
						  <div class="col-md-6">
						    <label for="exampleInputLastName">{{ trans('admin.ar_title') }}: </label>
						    <input class="form-control" id="title-ar" name="ar_title" type="text" aria-describedby="nameHelp" placeholder="{{ trans('admin.ar_title') }}" value="{{ $service->ar_title or '' }}">
						  </div>
						</div>
						</div>
						<div class="form-group">
						<div class="form-row">
						<div class="col-md-6">
						<label for="exampleInputName">{{ trans('admin.en_des') }}: </label>
						<textarea class="form-control" id="desc-en" name="en_des" type="text" aria-describedby="nameHelp" placeholder="{{ trans('admin.en_des') }}">{{ $service->en_des or '' }}</textarea>
						</div>
						<div class="col-md-6">
						<label for="exampleInputLastName">{{ trans('admin.ar_des') }}: </label>
						<textarea class="form-control" id="desc-en" name="ar_des" type="text" aria-describedby="nameHelp" placeholder="{{ trans('admin.ar_des') }}">{{ $service->ar_des or '' }}</textarea>
						</div>
						</div>
						</div>
						<div class="form-group">
						<label for="sel1">{{ trans('admin.icon') }}:</label>
						<select class="form-control fontaw" id="sel1" name="icon">
							<option value="fa fa-adjust">&#xf042</option>
							<option value="fa fa-anchor">&#xf13d</option>
							<option value="fa fa-angellist">&#xf209</option>
							<option value="fa fa-archive">&#xf187</option>
							<option value="fa fa-area-chart">&#xf1fe</option>
							<option value="fa fa-arrows-alt">&#xf0b2</option>
							<option value="fa fa-asl-interpreting">&#xf2a3</option>
							<option value="fa fa-asterisk"> &#xf069</option>
							<option value="fa fa-balance-scale">&#xf24e</option>
							<option value="fa fa-bank">&#xf19c</option>
							<option value="fa fa-bath">&#xf2cd</option>
							<option value="fa fa-bed">&#xf236</option>
							<option value="fa fa-beer">&#xf0fc</option>
							<option value="fa fa-bitbucket">&#xf171</option>
							<option value="fa fa-bookmark">&#xf02e</option>
							<option value="fa fa-briefcase">&#xf0b1</option>
							<option value="fa fa-building">&#xf1ad</option>
							<option value="fa fa-bullhorn">&#xf0a1</option>
							<option value="fa fa-bullseye">&#xf140</option>
							<option value="fa fa-car">&#xf1b9</option>
							<option value="fa fa-certificate">&#xf0a3</option>
							<option value="fa fa-check">&#xf00c</option>
							<option value="fa fa-child">&#xf1ae</option>
							<option value="fa fa-clock-o">&#xf017</option>
							<option value="fa fa-cloud">&#xf0c2</option>
							<option value="fa fa-code-fork">&#xf126</option>
							<option value="fa fa-cog">&#xf013</option>
						</select>
						</div>
	                </div>
	              	<div class="form-group">
	                	<button class="btn btn-success btn-block" type="submit">{{ trans('admin.save_btn') }}</button>
	              	</div>
	            </form>
	      	</div>
	 	</div>
	  
	</div>
</div>
 
@endsection('content')

@section('js')
<script type="text/javascript">
	$(function()
	{

		'use strict';

		//mark icon selected
		$('#sel1 option[value="{!! $service->icon or '' !!}"]').attr("selected", "selected");
	});
$('form').submit(function() 
{
        //send null values
        $(this).find(":input").filter(function(){return !this.value;}).value(null);
        $(this).find("textarea").filter(function(){return !this.value;}).value(null);

        $('form').find('input[type="file"]').filter(function(){return !this.value;}).value('');

});	
</script>
@endsection('js')