@extends('admin.layouts.app')
@section('content')
<div class="row">
        <div class="col-md-12">
          <div class="wrapper offset-md-2 col-md-8">
            <div class="card card-register mx-auto mt-5">
              <div class="card-header">{{trans('admin.about_header')}}</div>
              <div class="card-body">
                <form method="POST" enctype="multipart/form-data">
                  {{ csrf_field() }}
                        <div class="form-group">
                          <div class="form-row">
                            <div class="col-md-6">
                              <label for="exampleInputName">{{ trans('admin.en_title') }}:</label>
                              <input class="form-control" id="title-en" name="about_en_title" type="text" aria-describedby="nameHelp" placeholder="{{ trans('admin.en_title') }}" value="{{$arr['en_title'] or ''}}">
                            </div>
                            <div class="col-md-6">
                              <label for="exampleInputLastName">{{ trans('admin.ar_title') }}:</label>
                              <input class="form-control" id="title-ar" name="about_ar_title" type="text" aria-describedby="nameHelp" placeholder="{{ trans('admin.ar_title') }}" value="{{$arr['ar_title'] or ''}}">
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="form-row">
                            <div class="col-md-6">
                              <label for="exampleInputName">{{ trans('admin.en_des') }}:</label>
                              <textarea class="form-control" id="desc-en" name="about_en_des" type="text" aria-describedby="nameHelp" placeholder="{{ trans('admin.en_des') }}">{{$arr['en_des'] or ''}}</textarea>
                            </div>
                            <div class="col-md-6">
                              <label for="exampleInputLastName">{{ trans('admin.ar_des') }}:</label>
                              <textarea class="form-control" id="desc-en" name="about_ar_des" type="text" aria-describedby="nameHelp" placeholder="{{ trans('admin.ar_des') }}">{{$arr['ar_des'] or ''}}</textarea>
                            </div>
                          </div>
                        </div>
                        <div class="form-group " style="display: none;">
                          <label for="exampleInputEmail1">{{ trans('image') }}:</label>
                          <input class="form-control upload" id="pic" name="about_img" type="file" accept="image/jpg,image/jpeg,image/png">
                          <div id='img_contain'><img id="blah" align='middle' src="/uploads/app/{{$arr['img'] or ''}}" alt="your image" title=' upload a new image'/>
                          </div>
                        </div>
                        <div class="form-group">
                          <button class="btn btn-success btn-block" type="submit">{{ trans('admin.save_btn') }}</button>
                        </div>
                </form>
                
              </div>
            </div>
          </div>
        </div>
</div>
@endsection('content')
@section('js')
<script type="text/javascript">
function readURL(el,input) {

  if (input.files && input.files[0]) 
  {
    var reader = new FileReader();

    reader.onload = function(e) {
      el.next().find('img').attr('src', e.target.result);

      el.next().find('img').hide();
      el.next().find('img').fadeIn(650);

    }

    reader.readAsDataURL(input.files[0]);
  }
}

$(".upload").change(function() {
  readURL($(this),this);
});
$('form').submit(function() 
{
        //send null values
        $(this).find(":input").filter(function(){return !this.value;}).value(null);
        $(this).find("textarea").filter(function(){return !this.value;}).value(null);

        $('form').find('input[type="file"]').filter(function(){return !this.value;}).value('');

});
</script>
@endsection('js')