@extends('admin.layouts.app')
@section('content')
<div class="row">
        <div class="col-md-12">
            <div class="card card-register mx-auto mt-5">
              <div class="card-header">{{trans('admin.user_header')}}</div>
              <div class="card-body">
                <form method="POST" >
                	{{ csrf_field() }}
                      <div class="form-group">
                        <div class="form-row">
                          <div class="col-md-6 {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label for="exampleInputName">{{trans('admin.user_name')}}: </label>
                            <input class="form-control" id="name" name="name" type="text" aria-describedby="nameHelp" placeholder="{{trans('admin.user_name')}}" required="" value="{{$user->name or Request::old('name')}}">
                          </div>
                          <div class="col-md-6 {{ $errors->has('email') ? 'has-error' : '' }}">
                            <label for="exampleInputLastName">{{trans('admin.user_email')}}: </label>
                            <input class="form-control" id="email" name="email" type="email" aria-describedby="nameHelp" placeholder="{{trans('admin.user_email')}}" required="" value="{{$user->email or Request::old('email')}}">
                          </div>
                          <div class="col-md-12 {{ $errors->has('phone') ? 'has-error' : '' }}">
                            <label for="exampleInputLastName">{{trans('admin.user_phone')}}: </label>
                            <input class="form-control" id="phone" name="phone" type="number" aria-describedby="nameHelp" placeholder="{{trans('admin.user_phone')}}" required="" value="{{$user->phone or Request::old('phone')}}">
                          </div>
                          <div class="col-md-6 {{ $errors->has('password') ? 'has-error' : '' }}">
                            <label for="exampleInputLastName">{{trans('admin.user_pass')}}: </label>
                            <input class="form-control" id="pass" name="password" type="password" aria-describedby="nameHelp" placeholder="{{trans('admin.user_pass')}}" required="" >
                          </div>
                          <div class="col-md-6 {{ $errors->has('password') ? 'has-error' : '' }}">
                            <label for="exampleInputLastName">{{trans('admin.user_repeat_pass')}}: </label>
                            <input class="form-control" id="pass2" name="password_confirmation" type="password" aria-describedby="nameHelp" placeholder="{{trans('admin.user_repeat_pass')}}" required="">
                          </div>
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <button class="btn btn-success btn-block" type="submit">{{trans('admin.save_btn')}}</button>
                      </div>
                </form>
                
              </div>
            </div>
          
        </div>
</div>
@endsection('content')
@section('js')
<script type="text/javascript">
$('form').submit(function() 
{
        //send null values
        $(this).find(":input").filter(function(){return !this.value;}).value(null);
        $(this).find("textarea").filter(function(){return !this.value;}).value(null);

        $('form').find('input[type="file"]').filter(function(){return !this.value;}).value('');

});
</script>
@endsection('js')