@extends('admin.layouts.app')
@section('css')
<style type="text/css">
	.thumb-image
	{
		margin-right: 5px;
		margin-left: 5px;
	}
</style>
@endsection('css')
@section('content')
<div class="row">
	<div class="col-md-12">
	  	<div class="wrapper offset-md-1 col-md-10">
	        <div class="card card-register mx-auto mt-5">
	          	<div class="card-header">{{trans('admin.unit_header')}}</div>
	          	<div class="card-body">
	                <form method="POST"  enctype="multipart/form-data">
	                	{{ csrf_field() }}
                      	<div class="form-group">
	                        <div class="form-row">
	                          	<div class="col-md-12">
		                            <label for="exampleInputName">{{ trans('admin.unit_title') }}: </label>
		                            <input class="form-control" id="title" name="title" type="text" aria-describedby="nameHelp" placeholder="{{ trans('admin.unit_title') }}" required="" value="{{ $unit->title or '' }}">
	                          	</div>
		                      	<div class="col-md-12">
			                        <label for="exampleInputLastName">{{ trans('admin.unit_des') }}: </label>
			                        <textarea class="form-control"  placeholder="{{ trans('admin.unit_des') }}" name="description" required="">{{ $unit->description or '' }}</textarea>
		                      	</div>
	                          	<div class="col-md-6">
		                            <label for="exampleInputLastName">{{ trans('admin.unit_price') }}: </label>
		                            <input class="form-control" id="price" name="price" type="number" aria-describedby="nameHelp" placeholder="{{ trans('admin.unit_price') }}" required="" value="{{ $unit->price or '' }}">
	                          	</div>
	                          	<div class="col-md-6">
		                            <label for="exampleInputLastName">{{ trans('admin.unit_size') }}: </label>
		                            <input class="form-control" id="size" name="size" type="number" aria-describedby="nameHelp" placeholder="{{ trans('admin.unit_size') }}" required="" value="{{ $unit->size or '' }}">
	                          	</div>
	                          	<div class="col-md-6">
		                            <div class="form-group">
	                              		<label for="sel1">{{ trans('admin.unit_type') }}:</label>
	                              		<select class="form-control " id="type" name="type" required="">
	                              			@php
												$types = trans('app.unit_type');
												
	                              			@endphp 

											@foreach( $types as $key => $type )
												
												@php $selected = ''; @endphp

												@if(!empty($unit->type) and $unit->type == $key)

													@php $selected = 'selected="selected"'; @endphp
												@endif
												<option value="{{ $key }}" {!!$selected!!} > {{ $type }}</option>
											@endforeach
                              			</select>
		                            </div>
	                          	</div>
	                          	<div class="col-md-6">
		                            <div class="form-group">
	                              		<label for="sel1">{{ trans('admin.unit_section') }}:</label>
	                              		<select class="form-control " id="section" name="section_id" required="">

											@foreach( $sections as $key => $section )

												@php $selected = ''; @endphp

												@if(!empty($unit->section_id) and $unit->section_id == $key)

													@php $selected = 'selected="selected"'; @endphp
												@endif


												<option value="{{ $key }}" {!!$selected!!}>{{ $section[app('lang').'_section'] }}</option>

											@endforeach
                              			</select>
		                            </div>
	                          	</div>
	                          	<div class="col-md-6">
		                            <label for="exampleInputLastName">{{ trans('admin.unit_rooms') }}: </label>
		                            <input class="form-control" id="rooms" name="rooms" type="number" aria-describedby="nameHelp" placeholder="{{ trans('admin.unit_rooms') }}" value="{{ $unit->rooms or '' }}">
	                          	</div>
	                          	<div class="col-md-6">
		                            <label for="exampleInputLastName">{{ trans('admin.unit_bathrooms') }}: </label>
		                            <input class="form-control" id="bathrooms" name="bathrooms" type="number" aria-describedby="nameHelp" placeholder="{{ trans('admin.unit_bathrooms') }}" value="{{ $unit->bathrooms or '' }}" >
	                          	</div>
	                          	<div class="col-md-6">
		                            <label for="exampleInputLastName">{{ trans('admin.unit_floor') }}: </label>
		                            <input class="form-control" id="floors" name="floor" type="number" aria-describedby="nameHelp" placeholder="{{ trans('admin.unit_floor') }}" value="{{ $unit->floor or '' }}" >
	                          	</div>
	                          	<div class="col-md-6">
		                            <div class="form-group">
		                              <label for="sel2">{{ trans('admin.unit_view') }}:</label>
                              			<select class="form-control " id="sel2" name="view" >
	                              			@php
											$views = trans('app.unit_view')
	                              			@endphp 
											@foreach( $views as $key => $view )

												@php $selected = ''; @endphp

												@if(!empty($unit->type) and $unit->type == $key)

													@php $selected = 'selected="selected"'; @endphp
												@endif

												<option value="{{ $key }}" {!!$selected!!}>{{ $view }}</option>
											@endforeach
	                              		</select>
		                            </div>
	                          	</div>
	                          	<div class="col-md-6">
		                            <label for="exampleInputLastName">{{ trans('admin.unit_build_year') }}: </label>
		                            <input class="form-control" id="bdate" name="build_year" type="number" aria-describedby="nameHelp" placeholder="{{ trans('admin.unit_build_year') }}" value="{{ $unit->build_year or '' }}" >
	                          	</div>
	                          	<div class="col-md-6">
		                            <label for="exampleInputLastName">{{ trans('admin.unit_delivery_date') }}: </label>
		                            <input class="form-control" id="rdate" type="date" name="delivery_date" aria-describedby="nameHelp" placeholder="{{ trans('admin.delivery_date') }}" value="{{ $unit->delivery_date or '' }}" >
	                          	</div>
	                      		<div class="col-md-12">
		                            <div class="form-group">
		                              <label for="sel3">{{ trans('admin.unit_pay_type') }}:</label>
		                              <select class="form-control " id="sel3" name="pay_type">
	                              			@php
											$pay_types = trans('app.unit_pay_type')
	                              			@endphp 
											@foreach( $pay_types as $key => $pay_type )

												@php $selected = ''; @endphp

												@if(!empty($unit->pay_type) and $unit->pay_type == $key)

													@php $selected = 'selected="selected"'; @endphp
												@endif

												<option value="{{ $key }}" {!!$selected !!} >{{ $pay_type }}</option>
											@endforeach
		                              </select>
		                            </div>
	                          	</div>
	                          	<!-- location -->
	                      		<div class="col-md-12">
		                            <div class="form-group">
		                              <label for="sel3">{{trans('admin.unit_loaction')}}</label>
		                              <select class="form-control location" name="level_0" id="level_0" required="">
										
		                              </select>
		                            </div>
		                            <div class="form-group">
		                              
		                              <select class="form-control location" name="level_1" id="level_1" style="display: none;">

		                              </select>
		                            </div>
		                            <div class="form-group">
		                              
		                              <select class="form-control location" name="level_2" id="level_2" style="display: none;">

		                              </select>
		                            </div>
		                            <div class="form-group">
		                              
		                              <select class="form-control location" name="level_3" id="level_3" style="display: none;">

		                              </select>
		                            </div>
		                            <input type="hidden" id="location_holder" name="location_id">
	                          	</div>
	                          	<!--end location -->
	                          	<div class="col-md-12">
		                            <label for="exampleInputLastName">{{trans('admin.unit_address')}}: </label>
		                            <textarea class="form-control"  placeholder="{{trans('admin.unit_address')}}" name="address" >
		                            	{{ $unit->address or '' }}
		                            </textarea>
	                          	</div>

	                          	<div class="col-md-12">
				                      	<div id="drop-zone" style="margin-top: 10px; margin-bottom: 10px; ">
										    
										    <div id="clickHere"> <i class="fa fa-upload"></i>
										        <input type="file" name="images[]" id="file" multiple 
										        {{!empty($unit->id) ? '' : 'required=""' }} 
										        accept="image/*"/>
										    </div>
										    <div id='gallery' style="margin-top: 10px">
										    	@if(!empty($unit))
										    	@foreach($unit->images() as $key => $img)
										    	
													<img src="{{$unit->img_url($key)}}" class="thumb-image" width="100" height="100">
										    	
										    	@endforeach
										    	@endif
										    </div>
										</div>
	                              	
	                          	</div>
	                          	<div class="col-md-12 gallery" style="margin-bottom: 10px; "></div>
	                        </div>
                      	</div>
                      	<div class="form-group">
                        	<button class="btn btn-success btn-block" type="submit">{{trans('admin.save_btn')}}</button>
                      	</div>
	                </form>
	          	</div>
	        </div>
	  	</div>
	</div>
</div>
@endsection('content')
@section('js')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
$(document).ready(function() 
{
        $("#file").on('change', function() {
		  	if( this.files.length > 4)
		  	{
		  		alert('maximum number of allowed images for unit is 4 images');
		  		e.preventDefault();
		  		
		  	}
		  	else
		  	{
		  		$('#gallery').empty();
	          //Get count of selected files
	          var countFiles = $(this)[0].files.length;
	          var imgPath = $(this)[0].value;
	          var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
	          var image_holder = $(".gallery");
	          image_holder.empty();
	          if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
	            if (typeof(FileReader) != "undefined") {
	              //loop for each file selected for uploaded.
	              for (var i = 0; i < countFiles; i++) 
	              {
	                var reader = new FileReader();
	                reader.onload = function(e) {
	                  $("<img />", {
	                    "src": e.target.result,
	                    "class": "thumb-image",
	                    "width":100,
	                    "height":100,
	                  }).appendTo(image_holder);
	                }
	                image_holder.show();
	                reader.readAsDataURL($(this)[0].files[i]);
	              }
	            } else {
	              alert("This browser does not support FileReader.");
	            }
	          } else {
	            alert("Pls select only images");
	          }
	      	}
        });
	$('#drop-zone').click(function(){
		$('#file').click();
	});

});

</script>
<script>

	//locations handle

	//get locations 
	var locations = {!! $locations !!};
	var level = [];

	//helper function to filter locations
	function filter_locations(vars){

		return vars[this[0]] == this[1];
	}

	//helper function to fill select
	function fill_locations(el,arr)
	{
		el.empty();
		el.append($('<option disabled="disabled" selected="selected" value="">Select Location</option>'));
		for(var i in arr)
		{
			var location = arr[i];
			var opt = $('<option value="'+location["id"]+'">'+location["{{app('lang')}}_title"]+'</option>');

			el.append(opt);
		}
	}	


	//get main locations and fill it to select elment
	level[0] = locations.filter(filter_locations,['level',0]);
	fill_locations($('#level_0'),level[0]);

	$( ".location" ).each(function( index ) {
		$(this).change(function()
		{
			var next_index = index+1;
			child_locations = locations.filter(filter_locations,['parent_id',$(this).val()]);
			child_locations = child_locations.filter(function(vars){return !!vars['parent_id']});
			var child_level = $(".location").eq(next_index);

			//check if there is child locations
			if(child_locations.length)
			{
				child_level.show().attr("required","required");
				fill_locations(child_level,child_locations);

			}
			else if( next_index < $(".location").length)
			{
				child_level.empty();
				child_level.hide().removeAttr("required");
				
			}
			//run change to run trigger to next
			child_level.change();

			//store the actual location id in the holder
			$('#location_holder').val($(this).val());
		});
	});

</script>
@endsection('js')