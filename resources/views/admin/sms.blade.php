@extends('admin.layouts.app')
@section('content')
<div class="row">
        <div class="col-md-12">
            
            <div class="card card-register mx-auto mt-5">
              <div class="card-header">{{trans('admin.sms_header')}}</div>
              <div class="card-body">
                <form method="POST" >
                  {{ csrf_field() }}
                      <div class="form-group">
                        <div class="form-row">
                          <div class="col-md-12 text-center {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label for="exampleInputName">{{trans('admin.sms_remaining')}}: {{$sms_credit->value or ''}}</label>
                          </div>
                          <div class="col-md-12 {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label for="exampleInputName">{{trans('admin.sms_phone')}}: </label>
                            <input class="form-control" id="name" name="phone" type="text" aria-describedby="nameHelp" placeholder="{{trans('admin.sms_phone')}}" required="" pattern="(01[0,1,2,5]{1}[0-9]{8}|٠۱[٠,۱,٢,۵]{1}[٠-۹]{8})" value="{{Request::old('name')}}">
                          </div>

                          <div class="col-md-12 {{ $errors->has('msg') ? 'has-error' : '' }}">
                            <label for="exampleInputName">{{trans('admin.sms_msg')}}: </label>
                            <textarea class="form-control" required="" id="msg" rows="5" name="msg">{{Request::old('msg')}}</textarea>
                          </div>


                        </div>
                      </div>
                      
                      <div class="form-group">
                        <button class="btn btn-success btn-block" type="submit">{{trans('admin.send_btn')}}</button>
                      </div>
                </form>
                
              </div>
            </div>
        </div>
</div>
@endsection('content')
