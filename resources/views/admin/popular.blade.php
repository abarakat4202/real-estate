@extends('admin.layouts.app')
@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.css" />
@endsection('css')
@section('content')
<div class="row">
	<div class="col-md-12">
	  <div class="wrapper offset-md-2 col-md-8">
	    <div class="card card-register mx-auto mt-5">
	      <div class="card-header">{{ trans('admin.most_popular') }}</div>
	      <div class="card-body">
		       <form method="POST">
		       		{{ csrf_field() }}
		          	<div class="row">
			          <div class="col-md-2">
			            <h4>{{trans('admin.unit_1')}}</h4>
			          </div>
			          <div class="col-md-8">
			            <select id="e1" class="form-control" name="unit1">
							@foreach($units as $unit)
								@php 
									//check if this unit is marked before as pobular or not
									$marker = $unit->id == $populars[0]->value ? 'selected' : '';
								@endphp
								<option value="{{ $unit->id }}" {{ $marker }}>{{ $unit->title }}</option>
							@endforeach
			            </select>
			          </div>
			        </div>
			        <div class="row">
			          <div class="col-md-2">
			            <h4>{{trans('admin.unit_2')}}</h4>
			          </div>
			          <div class="col-md-8">
			            <select id="e2"  class="form-control"  name="unit2">
							@foreach($units as $unit)
								@php 
									//check if this unit is marked before as pobular or not
									$marker = $unit->id == $populars[1]->value ? 'selected' : '';
								@endphp
								<option value="{{ $unit->id }}" {{ $marker }}>{{ $unit->title }}</option>
							@endforeach
			            </select>
			          </div>
			        </div>
			        <div class="row">
			          <div class="col-md-2">
			            <h4>{{trans('admin.unit_3')}}</h4>
			          </div>
			          <div class="col-md-8">
			            <select id="e3"  class="form-control"  name="unit3">
							@foreach($units as $unit)
								@php 
									//check if this unit is marked before as pobular or not
									$marker = $unit->id == $populars[2]->value ? 'selected' : '';
								@endphp
								<option value="{{ $unit->id }}" {{ $marker }}>{{ $unit->title }}</option>
							@endforeach
			            </select>
			          </div>
			        </div>
			        <button style="margin-top: 20px;" type="submit" class="btn btn-success btn-block">{{ trans('admin.save_btn') }}</button>
		       </form>
	      </div>
    	</div>
	  </div>
	</div>
</div>

@endsection('content')

@section('js')
<script>
    $(document).ready(function() { 
      $("#e1").select2(); 
      $("#e2").select2(); 
      $("#e3").select2(); 
    });
$('form').submit(function() 
{
        //send null values
        $(this).find(":input").filter(function(){return !this.value;}).value(null);
        $(this).find("textarea").filter(function(){return !this.value;}).value(null);

        $('form').find('input[type="file"]').filter(function(){return !this.value;}).value('');

});    
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.js"></script>
@endsection('js')
