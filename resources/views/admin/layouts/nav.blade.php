<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
  <a class="navbar-brand" href="{{ Route('admin_home',app('lang')) }}">{{trans('admin.dashboard')}}</a>
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarResponsive">
    <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
      <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
        <a class="nav-link" href="{{ Route('admin_home',app('lang')) }}">
          <i class="fa fa-fw fa-dashboard"></i>
          <span class="nav-link-text">{{trans('admin.dashboard')}}</span>
        </a>
      </li>
      <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
        <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents" data-parent="#exampleAccordion">
          <i class="fa fa-fw fa-wrench"></i>
          <span class="nav-link-text">{{trans('admin.settings')}}</span>
        </a>
        <ul class="sidenav-second-level collapse" id="collapseComponents">
          <li>
            <a href="{{ Route('global_settings',app('lang')) }}">{{trans('admin.global_settings')}}</a>
          </li>
          <li>
            <a href="{{ Route('admin_slider',app('lang')) }}">{{trans('admin.slider')}}</a>
          </li>
          <li>
            <a href="{{ Route('admin_popular',app('lang')) }}">{{trans('admin.most_popular')}}</a>
          </li>
          <li>
            <a href="{{ Route('admin_about',app('lang')) }}">{{trans('admin.about')}}</a>
          </li>
        </ul>
      </li>

      <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Example Pages">
        <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseExamplePages" data-parent="#exampleAccordion">
          <i class="fa fa-fw fa-forumbee"></i>
          <span class="nav-link-text">{{trans('admin.units')}}</span>
        </a>
        <ul class="sidenav-second-level collapse" id="collapseExamplePages">
          <li>
            <a href="{{ Route('admin_add_unit',app('lang')) }}">{{trans('admin.add_unit')}}</a>
          </li>
          <li>
            <a href="{{ Route('admin_all_units',app('lang')) }}">{{trans('admin.all_unit')}}</a>
          </li>
        </ul>
      </li>
      <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Example Pages">
        <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseUsers" data-parent="#exampleAccordion">
          <i class="fa fa-fw fa-group"></i>
          <span class="nav-link-text">{{trans('admin.users')}}</span>
        </a>
        <ul class="sidenav-second-level collapse" id="collapseUsers">
          <li>
            <a href="{{ Route('admin_add_user',app('lang')) }}">{{trans('admin.add_user')}}</a>
          </li>
          <li>
            <a href="{{ Route('admin_all_users',app('lang')) }}">{{trans('admin.all_users')}}</a>
          </li>
        </ul>
      </li>
      <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
        <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseServices" data-parent="#exampleAccordion">
          <i class="fa fa-fw fa-flag"></i>
          <span class="nav-link-text">{{trans('admin.services')}}</span>
        </a>
        <ul class="sidenav-second-level collapse" id="collapseServices">
          <li>
            <a href="{{ Route('admin_add_service',app('lang')) }}">{{trans('admin.add_service')}}</a>
          </li>
          <li>
            <a href="{{ Route('admin_all_services',app('lang')) }}">{{trans('admin.all_services')}}</a>
          </li>
        </ul>
      </li>
      <li class="nav-item" data-toggle="tooltip" data-placement="right" title="" data-original-title="Tables">
          <a class="nav-link" href="{{route('admin_inquires',app('lang'))}}">
            <i class="fa fa-fw fa-commenting-o"></i>
            <span class="nav-link-text">{{trans('admin.inquires')}}</span>
          </a>
      </li>     
      <li class="nav-item" data-toggle="tooltip" data-placement="right" title="" data-original-title="Tables">
          <a class="nav-link" href="{{route('admin_send_sms',app('lang'))}}">
            <i class="fa fa-fw  fa-paper-plane-o"></i>
            <span class="nav-link-text">{{trans('admin.send_msg')}}</span>
          </a>
      </li> 
      <li class="nav-item" data-toggle="tooltip" data-placement="right" title="" data-original-title="Tables">
        <a class="nav-link" href="/lang/@if(app('lang')=='ar'){{'en'}}@else{{'ar'}}@endif">
          
          <span class="nav-link-text">@if(app('lang')=='ar'){{'English'}}@else{{'عربى'}}@endif</span
        </a>
      </li>       
    </ul>
    <ul class="navbar-nav sidenav-toggler">
      <li class="nav-item">
        <a class="nav-link text-center" id="sidenavToggler">
          <i class="fa fa-fw fa-angle-left"></i>
        </a>
      </li>
    </ul>
    <ul class="navbar-nav ml-auto">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle mr-lg-2" id="messagesDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fa fa-fw fa-envelope"></i>

          <span class="indicator text-primary d-none d-lg-block">
            <i class="fa fa-fw fa-circle"></i>
          </span>
        </a>
        <div class="dropdown-menu" aria-labelledby="messagesDropdown">
          <h6 class="dropdown-header">{{trans('admin.new_messages')}}:</h6>
          @foreach($notifications['contact'] as $notify)
          <div class="dropdown-divider"></div>
          <div class="dropdown-item notify" href="#" data-message="asdasdsa">
            <strong>{{$notify->name}}</strong>
            <span class="small float-right text-muted">{{ $notify->created_at }}</span>
            <div class="dropdown-message small">{{ $notify->message }}</div>
          </div>
          @endforeach
          <div class="dropdown-divider"></div>
          <a class="dropdown-item small" href="{{route('admin_inquires',app('lang'))}}">View all messages</a>
      </li>      
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle mr-lg-2" id="alertsDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fa fa-fw fa-bell"></i>
          <span class="indicator text-warning d-none d-lg-block">
              <i class="fa fa-fw fa-circle"></i>
          </span>
        </a>
        <div class="dropdown-menu" aria-labelledby="alertsDropdown" style="width: 50%;">
          <h6 class="dropdown-header">{{trans('admin.new_alerts')}}:</h6>

          @foreach($notifications['alerts'] as $notify)
          <div class="dropdown-divider"></div>
          <div class="dropdown-item notify" >
            <span class="text-success">
              <strong>
                <i class="fa fa-long-arrow-up fa-fw"></i>{{ $notify->unit->title or ''}}</strong>
            </span>
            <span class="small float-right text-muted">{{ $notify->created_at }}</span>
            <div class="dropdown-message small">{{ $notify->message }}</div>
          </div>
          @endforeach
         
          <div class="dropdown-divider"></div>
          <a class="dropdown-item small" href="{{route('admin_inquires',app('lang'))}}">{{trans('admin.view_alerts')}}</a>
        </div>
      </li>
      <li class="nav-item">
              <span class="nav-link" style="color:white;"><i class="fa fa-fw fa-user" ></i>{{auth()->user()->name}}</span>

      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
          <i class="fa fa-fw fa-sign-out"></i>{{trans('admin.logout_btn')}}</a>
      </li>
    </ul>
  </div>
</nav>

<div class="modal fade" id="notify" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
        <h4 class="modal-title custom_align" id="Heading">{{trans('admin.msg_heading')}}</h4>
      </div>

      <div class="modal-notify">
        
      </div>

    </div>
  </div>
</div>
@section('js')
<script type="text/javascript">
$(function(){
  'use strict';

  //create trigger to add id of item to modal box
  $('.notify').click(function(){


    $('.modal-notify').html($(this).attr('data-message'));
  });
});
</script>
@endsection('js')