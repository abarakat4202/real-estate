<!DOCTYPE html>
<html lang="en">
<!-- head-->
@include('admin.layouts.head')
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  @auth
  <!-- Navigation-->
  @include('admin.layouts.nav')
  @endauth
  @yield('content')
  @auth
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © Your Website 2018</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">{{ trans('admin.logout_header') }}</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">{{ trans('admin.logout_msg') }}</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">{{ trans('admin.cancel') }}</button>
            <a class="btn btn-primary" href="/{{app('lang')}}/logout">{{ trans('admin.logout_btn') }}</a>
          </div>
        </div>
      </div>
    </div>
  @endauth  
  @include('admin.layouts.footer_scripts')
</body>

</html>
