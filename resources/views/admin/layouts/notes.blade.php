@if(count($errors))
<div class="container">
	<div class="form-group">
		<div class="alert alert-danger">
			<ul>
				@foreach($errors -> all() as $error)
					<li>{{$error}}</li>
				@endforeach
			</ul>
		</div>

	</div>
</div>
@endif

@if(!empty(Session::get('messages')))
<div class="container">
	<div class="form-group">
		<div class="alert alert-info">
			<ul>
				@foreach(Session::get('messages') as $msg)
					<li>{{$msg}}</li>
				@endforeach
			</ul>
		</div>

	</div>
</div>
@endif