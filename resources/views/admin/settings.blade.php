@extends('admin.layouts.app')
@section('content')
@section('css')
<style type="text/css">
  .img_contain
  {
    max-width : 200px;
    max-height: 200px;
    
  }
</style>
@endsection('css')
<div class="row">
    <div class="col-md-12">
      <div class="card card-register mx-auto mt-5">
        <div class="card-header">{{trans('admin.global_header')}}</div>
        <div class="card-body">
          <form method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
              <div class="form-row">
                <div class="col-md-6">
                  <label for="title">{{trans('admin.website_ar_title')}}:</label>
                  <input class="form-control" id="title" name="website_ar_title" type="text" aria-describedby="nameHelp" placeholder="{{trans('admin.website_ar_title')}}" value="{{ $arr['website_ar_title'] or '' }}">
                </div>
                <div class="col-md-6">
                  <label for="title">{{trans('admin.website_en_title')}}:</label>
                  <input class="form-control" id="title" name="website_en_title" type="text" aria-describedby="nameHelp" placeholder="{{trans('admin.website_en_title')}}" value="{{ $arr['website_en_title'] or '' }}">
                </div>
                <div class="col-md-12">
                  <label for="phone">{{trans('admin.website_phone')}}: </label>
                  <input class="form-control" id="phone" name="phone" type="number" aria-describedby="nameHelp" placeholder="{{trans('admin.website_ar_title')}}" value="{{ $arr['phone'] or '' }}">
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="address">{{trans('admin.website_ar_address')}}: </label>
              <input class="form-control" id="address" name="ar_address" type="text" aria-describedby="emailHelp" placeholder="{{trans('admin.website_ar_address')}}" value="{{ $arr['ar_address'] or '' }}">
            </div>
            <div class="form-group">
              <label for="address">{{trans('admin.website_en_address')}}: </label>
              <input class="form-control" id="address" name="en_address" type="text" aria-describedby="emailHelp" placeholder="{{trans('admin.website_en_address')}}" value="{{ $arr['en_address'] or '' }}">
            </div>
            <div class="form-group">
              <label for="fb">{{trans('admin.fb_page')}}:</label>
              <input class="form-control" id="fb" type="text" name="fb" aria-describedby="emailHelp" placeholder="{{trans('admin.fb_page')}}" value="{{ $arr['fb'] or ''}}">
            </div>

            <div class="form-group">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a class="btn btn-primary collapsed" data-toggle="collapse" data-parent="#accordion" href="#logo" aria-expanded="false">{{trans('admin.website_logo')}}</a>
                    </h4>
                  </div>
                  <div id="logo" class="panel-collapse in collapse" style="">
                    <input class="form-control upload" name="logo_img" id="logo_img" type="file" aria-describedby="emailHelp" >
                    <div id='img_contain'><img id="blah" align='middle' src="{{ asset('uploads/app/'.$arr['logo_img']) }}" alt="your image" title=' upload a new image'/>
                    </div>
                  </div>
                </div>
            </div>
            
              <div class="row">
                  <div class="col-md-6">
                  <div class="form-group">
                  <label for="slog-title">{{trans('admin.slogan_en_title')}}:</label>
                  <input class="form-control" id="slog-title-eb" name="slogan_en_title" type="text" aria-describedby="emailHelp" placeholder="{{trans('admin.slogan_en_title')}}" value="{{ $arr['slogan_en_title'] or '' }}">
                </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                  <label for="slog-title">{{trans('admin.slogan_ar_title')}}:</label>
                  <input class="form-control" id="slog-title" name="slogan_ar_title" type="text" aria-describedby="emailHelp" placeholder="{{trans('admin.slogan_ar_title')}}" value="{{ $arr['slogan_ar_title'] or '' }}">
                </div>
                </div>
              </div>
            <div class="form-group">
              <label for="slog-des">{{trans('admin.slogan_en_des')}}:</label>
              <textarea class="form-control" id="slog-des" name="slogan_en_des" placeholder="{{trans('admin.slogan_en_des')}}">{{ $arr['slogan_en_des'] or '' }}</textarea>
            </div>
            <div class="form-group">
              <label for="slog-des">{{trans('admin.slogan_ar_des')}}:</label>
              <textarea class="form-control" id="slog-des" name="slogan_ar_des" placeholder="{{trans('admin.slogan_ar_des')}}">{{ $arr['slogan_ar_des'] or '' }}</textarea>
            </div>

            <div class="form-group">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a class="btn btn-primary collapsed" data-toggle="collapse" data-parent="#accordion" href="#slogan" aria-expanded="false">
                      {{trans('admin.slogan_img')}}</a>
                    </h4>
                  </div>
                  <div id="slogan" class="panel-collapse in collapse" style="">
                    <input class="form-control upload" name="slogan_img" id="slogan_img" type="file" aria-describedby="emailHelp" >
                    <div id='img_contain'><img id="blah" align='middle' src="{{asset('uploads/app/'.$arr['slogan_img']) }}" alt="your image" title=' upload a new image'/>
                    </div>
                  </div>
                </div>
            </div>       
              
            <div class="form-group">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a class="btn btn-primary collapsed" data-toggle="collapse" data-parent="#accordion" href="#partner1" aria-expanded="false">
                      {{trans('admin.partner_1')}}</a>
                    </h4>
                  </div>
                  <div id="partner1" class="panel-collapse in collapse" style="">
                    <input class="form-control upload" name="partner1_img" id="partner1_img" type="file" aria-describedby="emailHelp" >
                    <div id='img_contain'><img id="blah" align='middle' src="{{asset('uploads/app/'.$arr['partner1_img']) }}" alt="your image" title=' upload a new image'/>
                    </div>
                  </div>
                </div>
            </div> 

            <div class="form-group">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a class="btn btn-primary collapsed" data-toggle="collapse" data-parent="#accordion" href="#partner2" aria-expanded="false">
                      {{trans('admin.partner_2')}}</a>
                    </h4>
                  </div>
                  <div id="partner2" class="panel-collapse in collapse" style="">
                    <input class="form-control upload" name="partner2_img" id="partner2_img" type="file" aria-describedby="emailHelp" >
                    <div id='img_contain'><img id="blah" align='middle' src="{{asset('uploads/app/'.$arr['partner2_img']) }}" alt="your image" title=' upload a new image'/>
                    </div>
                  </div>
                </div>
            </div> 

            <div class="form-group">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a class="btn btn-primary collapsed" data-toggle="collapse" data-parent="#accordion" href="#partner3" aria-expanded="false">
                      {{trans('admin.partner_3')}}</a>
                    </h4>
                  </div>
                  <div id="partner3" class="panel-collapse in collapse" style="">
                    <input class="form-control upload" name="partner3_img" id="partner3_img" type="file" aria-describedby="emailHelp" >
                    <div id='img_contain'><img id="blah" align='middle' src="{{asset('uploads/app/'.$arr['partner3_img']) }}" alt="your image" title=' upload a new image'/>
                    </div>
                  </div>
                </div>
            </div> 

            <div class="form-group">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a class="btn btn-primary collapsed" data-toggle="collapse" data-parent="#accordion" href="#partner4" aria-expanded="false">
                      {{trans('admin.partner_4')}}</a>
                    </h4>
                  </div>
                  <div id="partner4" class="panel-collapse in collapse" style="">
                    <input class="form-control upload" name="partner4_img" id="partner4_img" type="file" aria-describedby="emailHelp" >
                    <div id='img_contain'><img id="blah" align='middle' src="{{asset('uploads/app/'.$arr['partner4_img']) }}" alt="your image" title=' upload a new image'/>
                    </div>
                  </div>
                </div>
            </div> 

            <div class="form-group">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a class="btn btn-primary collapsed" data-toggle="collapse" data-parent="#accordion" href="#partner5" aria-expanded="false">
                      {{trans('admin.partner_5')}}</a>
                    </h4>
                  </div>
                  <div id="partner5" class="panel-collapse in collapse" style="">
                    <input class="form-control upload" name="partner5_img" id="partner5_img" type="file" aria-describedby="emailHelp" >
                    <div id='img_contain'><img id="blah" align='middle' src="{{asset('uploads/app/'.$arr['partner5_img']) }}" alt="your image" title=' upload a new image'/>
                    </div>
                  </div>
                </div>
            </div>                                     


            <input class="btn btn-success btn-block" type="submit" value="{{trans('admin.save_btn')}}">
          </form>
        </div>
      </div>
    </div>
</div>
@endsection('content')
@section('js')
<script type="text/javascript">
function readURL(el,input) {

  if (input.files && input.files[0]) 
  {
    var reader = new FileReader();

    reader.onload = function(e) {
      el.next().find('img').attr('src', e.target.result);

      el.next().find('img').hide();
      el.next().find('img').fadeIn(650);

    }

    reader.readAsDataURL(input.files[0]);
  }
}

$(".upload").change(function() {
  readURL($(this),this);
});

//handle empty inputs

$('form').submit(function() 
{
        //send null values
        $(this).find(":input").filter(function(){return !this.value;}).value(null);
        $(this).find("textarea").filter(function(){return !this.value;}).value(null);

        $('form').find('input[type="file"]').filter(function(){return !this.value;}).value('');

});
</script>
@endsection('js')