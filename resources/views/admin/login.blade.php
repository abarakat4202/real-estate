@extends('admin.layouts.sessions')
@section('content')
  <div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">{{ trans('admin.login_header') }}</div>
      <div class="card-body">
        <form method="POST">
          {{ csrf_field() }}
          <div class="form-group {{ $errors->has('login_method') ? 'has-error' : '' }}">
            <label for="exampleInputEmail1">{{ trans('admin.login_id_lbl') }}</label>
            <input class="form-control" id="exampleInputEmail1" type="text" aria-describedby="emailHelp" placeholder="{{ trans('admin.login_id_lbl') }}" name="login_method" value="{{ Request::old('login_method') }}">
          </div>
          <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
            <label for="exampleInputPassword1">{{ trans('admin.login_pass_lbl') }}</label>
            <input class="form-control" type="password" placeholder="{{ trans('admin.login_pass_lbl') }}" name="password" value="{{ Request::old('password') }}">
          </div>
          <div class="form-group" >
            <div class="form-check">
              <label class="form-check-label">
                <input class="form-check-input" name="remember" type="checkbox">{{trans('admin.remember_lbl')}}</label>
            </div>
          </div>
          <input type="submit" class="btn btn-primary btn-block" value="{{trans('admin.login_btn')}}" >
        </form>
        <div class="text-center">
          <!-- <a class="d-block small" href="forgot-password.html">Forgot Password?</a> -->
        </div>
      </div>
    </div>
  </div>
 
@endsection('content')
