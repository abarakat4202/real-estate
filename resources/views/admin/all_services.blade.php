@extends('admin.layouts.app')
@section('content')
<div class="row">

  <div class="col-md-12">
    <form method="get">

      <div class="row">
          <div class="col-md-8">
            <div class="input-group">
              <input class="form-control" name="query" value="{{ Request('query') }}" type="text" placeholder="{{trans('admin.search_statement')}}">
              <span class="input-group-append">
                <button class="btn btn-primary" type="submit">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div>
          </div>
          <div class="col-md-4 " hidden>
            <div class="dropdown">
              <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Sort
              <span class="caret"></span></button>
              <ul class="dropdown-menu">
                <li class="li"><a href="#">Ascending</a></li>
                <li class="li"><a href="#">Descending</a></li>
              </ul>
            </div>
          </div>
        
      </div>
    </form>
    <div class="table-responsive">    
          <table id="mytable" class="table table-bordred table-striped"> 
             <thead>
                <th>{{trans('admin.date_lbl')}}</th>
                <th>{{trans('admin.service_lbl')}}</th>
                <th>{{ trans('admin.added_by_lbl') }}</th>
                <th class="center" >{{ trans('admin.actions_lbl') }}</th>
             </thead>
             <tbody>
              @foreach($services as $service )
                <tr>
                  <td>{{ $service->created_at }}</td>
                  <td>{{ $service[app('lang').'_title'] }}</td>
                  <td>{{ $service->user->name }}</td>
                  <td class="center" >
                    <a href="{{Route('admin_edit_service',[app('lang'),$service->id])}}" target="_SELF" class="btn btn-primary">{{ trans('admin.edit_btn') }}</a>
                    <button class="btn btn-danger delete" data-title="Delete" data-id="{{ $service->id }}" data-toggle="modal" data-target="#delete">{{ trans('admin.delete_btn') }}</button>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        <div class="clearfix"></div>
    </div>
  </div>
</div>
{{ $services->appends(Request::input())->render("pagination::bootstrap-4") }} 
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
        <h4 class="modal-title custom_align" id="Heading">{{ trans('admin.del_header') }}</h4>
      </div>

      <div class="modal-body">
        <div class="alert alert-danger">
          <span class="glyphicon glyphicon-warning-sign"></span>
           {{ trans('admin.del_msg') }}
         </div>
      </div>
      <div class="modal-footer ">

        <a class="btn btn-danger" href="" id="modal_action"><span class="fa fa-edit "></span>{{trans('admin.confirm')}}</a>

        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-remove "></span>{trans('admin.not_confirm')}}</button>

      </div>

    </div>
  </div>
</div>
@endsection('content')
@section('js')
<script type="text/javascript">
$(function(){
  'use strict';

  //create trigger to add id of item to modal box
  $('.delete').click(function(){

    var delete_url = "{{Route('admin_delete_service',[app('lang'),'service_id'])}}";
    delete_url = delete_url.replace('service_id',$(this).attr('data-id'));
    $('#modal_action').attr("href",delete_url);
  });
});
</script>
@endsection('js')
