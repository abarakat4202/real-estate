<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*get lcations from aqarmap to store to database
Route::get('location','LocationController@store');
Route::get('location/update_en','LocationController@update_en');
Route::get('location/update_parent_id','LocationController@update_parent_id');
*/
//change lang
Route::get('lang/{lang}','LangController@changeLang');
Route::get(app('lang').'/admin/login','SessionsController@create')->name('login');	
Route::get(app('lang').'/login','SessionsController@create');



//admin perfix
Route::group(['middleware' => 'lang' , 'prefix' => '/{lang}/admin'],function($lang){

	//admin home
	Route::get('/','DashboardController@dasboard')->name('admin_home');

	//sessions
	Route::get('/login','SessionsController@create');	
	Route::post('/login','SessionsController@store');	
	Route::get('/logout','SessionsController@destroy');
	//global settings
	Route::get('/settings','DashboardController@settings_edit')->name('global_settings');
	Route::post('/settings','DashboardController@settings_store');
	//admin slider
	Route::get('/slider','DashboardController@slider_edit')->name('admin_slider');
	Route::post('/slider','DashboardController@slider_store');
	//most popular
	Route::get('/popular','DashboardController@popular_edit')->name('admin_popular');
	Route::post('/popular','DashboardController@popular_store');

	//about
	Route::get('/about','DashboardController@about_edit')->name('admin_about');
	Route::post('/about','DashboardController@about_store');

	/////////////////////////////
	//units///////////////////
	/////////////////////////////

	//all units
	Route::get('/units','DashboardController@all_units')->name('admin_all_units');

	//add unit
	Route::get('/units/add','DashboardController@add_unit')->name('admin_add_unit');
	Route::post('/units/add','DashboardController@store_unit');

	//edit unit
	Route::get('/units/{unit}/edit','DashboardController@edit_unit')->name('admin_edit_unit');
	Route::post('/units/{unit}/edit','DashboardController@update_unit');

	//delete unit
	Route::get('/units/{unit}/delete','DashboardController@delete_unit')->name('admin_delete_unit');

	/////////////////////////////
	//services///////////////////
	/////////////////////////////

	//all services
	Route::get('/services','DashboardController@all_services')->name('admin_all_services');

	//add service
	Route::get('/services/add','DashboardController@add_service')->name('admin_add_service');
	Route::post('/services/add','DashboardController@store_service');

	//edit service
	Route::get('/services/{service}/edit','DashboardController@edit_service')->name('admin_edit_service');
	Route::post('/services/{service}/edit','DashboardController@update_service');

	//delete service
	Route::get('/services/{service}/delete','DashboardController@delete_service')->name('admin_delete_service');


	/////////////////////////////
	//users///////////////////
	/////////////////////////////

	//all users
	Route::get('/users','DashboardController@all_users')->name('admin_all_users');

	//add user
	Route::get('/users/add','DashboardController@add_user')->name('admin_add_user');
	Route::post('/users/add','DashboardController@store_user');

	//edit user
	Route::get('/users/{user}/edit','DashboardController@edit_user')->name('admin_edit_user');
	Route::post('/users/{user}/edit','DashboardController@update_user');

	//delete user
	Route::get('/users/{user}/delete','DashboardController@delete_user')->name('admin_delete_user');

	/////////////////////////////
	//inquires///////////////////
	/////////////////////////////
	Route::get('/inquires','DashboardController@get_inquires')->name('admin_inquires');

	/////////////////////////////
	//sms///////////////////
	/////////////////////////////
	Route::get('/sms/send','DashboardController@send_sms')->name('admin_send_sms');	
	Route::post('/sms/send','DashboardController@post_sms')->name('admin_send_sms');	

});

//redirect index
Route::redirect('/',app('lang'));
Route::redirect('/index.html',app('lang'));
Route::redirect('/index.php',app('lang'));
Route::redirect('/index',app('lang'));

//base perfix
Route::group(['middleware' => 'lang' , 'prefix' => '/{lang}'],function($lang){

	//Route::get('/login','SessionsController@create')->name('login');	
	Route::post('/login','SessionsController@store');	
	Route::get('/logout','SessionsController@destroy');	

	//home page
	Route::get('/','UnitsController@index')->name('home');

	//show unit
	Route::get('/units/{unit}/{title?}','UnitsController@show')->name('unit_view')->where('unit','[0-9]+');

	//inquiry about unit
	Route::post('/units/{unit}/{title?}','InquiryController@create')->where('unit','[0-9]+');

	//all units
	Route::get('/units','UnitsController@all')->name('all_units');

	//about
	Route::get('/about','UnitsController@about')->name('about');

	//contact
	Route::get('/contact','UnitsController@contact')->name('contact');
	Route::post('/contact','InquiryController@contact');

	
});
