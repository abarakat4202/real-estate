<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Locations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ar_title');
            $table->string('en_title')->nullable();
            $table->string('slug')->nullable();
            $table->string('parent_slug')->nullable();
            $table->string('lat')->nullable();
            $table->string('lng')->nullable();
            $table->smallInteger('zoom_level')->nullable();
            $table->string('searchable')->nullable();
            $table->string('estimate')->nullable();
            $table->smallInteger('level')->nullable();
            $table->integer('parent_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Locations');
    }
}
