<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('role')->nullable();
            $table->string('email')->unique();
            $table->mediumText('phone');
            $table->string('password');
            $table->string('gender')->nullable();
            $table->string('martial_status')->nullable();
            $table->string('education')->nullable();
            $table->string('work_field')->nullable();
            $table->string('job')->nullable();
            $table->string('goal')->nullable();
            $table->string('lang')->nullable();
            $table->date('birthdate')->nullable();
            $table->mediumText('description')->nullable();
            $table->mediumText('logo')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
