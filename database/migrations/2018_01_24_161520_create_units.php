<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('units', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('location')->nullable(); //show but will use lat and lng in update           
            $table->string('title')->nullable(); //show
            $table->mediumText('description')->nullable(); //show
            $table->mediumText('video_url')->nullable(); //in update
            $table->integer('price')->nullable(); //show
            $table->string('section')->nullable(); //revision
            $table->integer('type_id')->nullable(); //show
            $table->string('advertiser_type')->nullable();
            $table->mediumText('phone')->nullable(); //hidden
            $table->mediumInteger('size')->nullable(); //show
            $table->tinyInteger('rooms')->nullable(); //show
            $table->tinyInteger('bathrooms')->nullable(); //show
            $table->tinyInteger('floor')->nullable(); //show
            $table->string('view')->nullable(); //show
            $table->string('pay_type')->nullable(); //show
            $table->string('build_year')->nullable(); //show not req
            $table->date('delivery_date')->nullable(); //show
            $table->string('address')->nullable(); //hidden
            $table->string('lat')->nullable(); //not used
            $table->string('lng')->nullable(); //not used
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('units');
    }
}
