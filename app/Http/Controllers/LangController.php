<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LangController extends Controller
{
    //
    function changeLang(Request $request,$lang)
    {


		if(in_array($lang,['ar','en']))
		{
			

			if(auth()->user())
			{
				$user = auth()->user();
				$user->lang = $lang;
				$user->save();
			}
			else
			{
				if(session()->has('lang'))
				{
					session()->forget('lang');
				}
				session()->put('lang',$lang);
			}
		}
		else
		{
			if(session()->has('lang'))
			{
				session()->forget('lang');
			}		
			session()->put('lang','en');
		}

		if(!empty(back()->getTargetUrl()))
		{
			return redirect(str_replace(array('/en/','/ar/','/en','/ar'), array('/'.$lang.'/','/'.$lang.'/','/'.$lang,'/'.$lang), back()->getTargetUrl()));
		}
		else
		{
			redirect()->home();
		}
	}

}
