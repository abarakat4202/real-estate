<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Upload;
use App\Variable;
use App\Unit;
use App\Section;
use App\Location;
use App\Service;
use App\User;
use App\Inquiry;
use Auth;
class DashboardController extends Controller
{
    //

    //make sure that user is authenticated
    public function __construct()
    {
        

        $notifications = [

            'alerts'      => Inquiry::where('inquiry_type','unit')
            ->orderBy('created_at','DESC')->limit(3)->get(),
            'contact'     => Inquiry::where('inquiry_type','contact')
            ->orderBy('created_at','DESC')->limit(3)->get(),
        ];
       

        view()->share(compact('notifications'));
        return $this->middleware('auth');

    }

    public function dasboard()
    {
        return view('admin.index');
    }

    //settings get handle 
    public function settings_edit()
    {
        //get values
        $arr = [

            'website_en_title'  => Variable::where('name','website_en_title')->first()->value,
            'website_ar_title'  => Variable::where('name','website_ar_title')->first()->value,
            'phone'             => Variable::where('name','phone')->first()->value,
            'en_address'        => Variable::where('name','en_address')->first()->value,
            'ar_address'        => Variable::where('name','ar_address')->first()->value,
            'fb'                => Variable::where('name','fb')->first()->value,
            'slogan_en_title'   => Variable::where('name','slogan_en_title')->first()->value,
            'slogan_ar_title'   => Variable::where('name','slogan_ar_title')->first()->value,
            'slogan_en_des'     => Variable::where('name','slogan_en_des')->first()->value,
            'slogan_ar_des'     => Variable::where('name','slogan_ar_des')->first()->value,
            'logo_img'          => Variable::where('name','logo_img')->first()->value,
            'slogan_img'        => Variable::where('name','slogan_img')->first()->value,
            'partner1_img'      => Variable::where('name','partner1_img')->first()->value,
            'partner2_img'      => Variable::where('name','partner2_img')->first()->value,
            'partner3_img'      => Variable::where('name','partner3_img')->first()->value,
            'partner4_img'      => Variable::where('name','partner4_img')->first()->value,
            'partner5_img'      => Variable::where('name','partner5_img')->first()->value,


        ];
    	return view('admin.settings',compact('arr'));
    }


    //settings post handle 
    public function settings_store(Request $req)
    {
        //validate form iputs
        $this->validate(request(),[

            'slogan_img'   => 'image|mimes:jpg,jpeg,png,gif',
            'logo_img'     => 'image|mimes:jpg,jpeg,png,gif',
            'partner1_img' => 'image|mimes:jpg,jpeg,png,gif',
            'partner2_img' => 'image|mimes:jpg,jpeg,png,gif',
            'partner3_img' => 'image|mimes:jpg,jpeg,png,gif',
            'partner4_img' => 'image|mimes:jpg,jpeg,png,gif',
            'partner5_img' => 'image|mimes:jpg,jpeg,png,gif',
        ]);

        //get request params
        $files  = request()->file();
        $inputs = request()->input();
        
        //loop in files and store them in public path
        foreach ($files as $key => $file) 
        {
            
            $tmp_file = new Upload($file);
            $extension = $tmp_file->ext;
            $tmp_file->move('uploads/app',$key.'.'.$extension);
            //store extension
            Variable::updateOrCreate(['name' => $key ],['value' => $key.'.'.$extension]);        
        }

        //loop in inputs to store in variables table
        foreach ($inputs as $key => $input) 
        {
            
            Variable::updateOrCreate(['name' => $key ],['value' => $input]);
        }
        \Session::flash('messages', [trans('admin.edit_success_msg')]);
        
    	return redirect()->route('global_settings',app('lang'));
    }
    //

    /////////////////////////////////
    /////// about us ////////////////
    /////////////////////////////////
    //about get handle 
    public function about_edit()
    {
        //get values
        $arr = [

            'en_title'     => Variable::where('name','about_en_title')->first()->value,
            'ar_title'             => Variable::where('name','about_ar_title')->first()->value,
            'en_des'           => Variable::where('name','about_en_des')->first()->value,
            'ar_des'                => Variable::where('name','about_ar_des')->first()->value,
            'img'      => Variable::where('name','about_img')->first()->value,
        ];

        return view('admin.about',compact('arr'));
    }


    //about post handle 
    public function about_store(Request $req)
    {
        //validate form iputs
        $this->validate(request(),[
            'about_img'   => 'image|mimes:jpg,jpeg,png,gif',

        ]);

        //get request params
        $files  = request()->file();
        $inputs = request()->input();   

        
        //loop in files and store them in public path
        foreach ($files as $key => $file) 
        {
                        $tmp_file = new Upload($file);
            $extension = $tmp_file->ext;
            $tmp_file->move('uploads/app/',$key.'.'.$extension);
            //store extension
            Variable::updateOrCreate(['name' => $key ],['value' => $key.'.'.$extension]);        
        }

        //loop in inputs to store in variables table
        foreach ($inputs as $key => $input) 
        {
            
            Variable::updateOrCreate(['name' => $key ],['value' => $input]);
        }
        \Session::flash('messages', [trans('admin.edit_success_msg')]);
        
        return redirect()->route('about',app('lang'));
    }




    public function slider_edit()
    {
    	$slider1 = array(
            'ar_title' => Variable::where('name','slider1_ar_title')->first(),
            'en_title' => Variable::where('name','slider1_en_title')->first(),
            'ar_des'   => Variable::where('name','slider1_ar_des')->first(),
            'en_des'   => Variable::where('name','slider1_en_des')->first(),
            'img'      => Variable::where('name','slider1_img')->first(),
        );
        $slider2 = array(
            'ar_title' => Variable::where('name','slider2_ar_title')->first(),
            'en_title' => Variable::where('name','slider2_en_title')->first(),
            'ar_des'   => Variable::where('name','slider2_ar_des')->first(),
            'en_des'   => Variable::where('name','slider2_en_des')->first(),
            'img'      => Variable::where('name','slider2_img')->first(),
        );      
        $slider3 = array(
            'ar_title' => Variable::where('name','slider3_ar_title')->first(),
            'en_title' => Variable::where('name','slider3_en_title')->first(),
            'ar_des'   => Variable::where('name','slider3_ar_des')->first(),
            'en_des'   => Variable::where('name','slider3_en_des')->first(),
            'img'      => Variable::where('name','slider3_img')->first(),
        );
    	return view('admin.slider',compact('slider1','slider2','slider3'));
    }
    public function slider_store(Request $req)
    {
        //validate form iputs
        $this->validate(request(),[

            'slider1_img' => 'image|mimes:jpg,jpeg,png,gif',
            'slider2_img' => 'image|mimes:jpg,jpeg,png,gif',
            'slider3_img' => 'image|mimes:jpg,jpeg,png,gif',
        ]);

        //get request params
        $files  = request()->file();
        $inputs = request()->input();
        
        //loop in files and store them in public path
        foreach ($files as $key => $file) 
        {
            
            $tmp_file = new Upload($file);
            $extension = $tmp_file->ext;
            $tmp_file->move('uploads/app',$key.'.'.$extension);

            //store extension
            Variable::updateOrCreate(['name' => $key ],['value' => $key.'.'.$extension]);
        }

        //loop in inputs to store in variables table
        foreach ($inputs as $key => $input) 
        {
            
            Variable::updateOrCreate(['name' => $key ],['value' => $input]);
        }
        \Session::flash('messages', [trans('admin.edit_success_msg')]);
    	return back();
    }


    public function popular_edit()
    {
    	//get populars ids
    	$populars = Variable::where('name','like','%popular%')->get();

    	$units = Unit::latest()->get();
    	return view('admin.popular',compact('units','populars'));
    }


    public function popular_store(Request $req)
    {

    	//store populars
    	$popular1 = Variable::firstOrNew(['name'=>'popular1']);
    	$popular1->value = request('unit1');
    	$popular1->save();
    	Variable::updateOrCreate(array('name'=>'popular2'),['value'=>request('unit2')]);
    	Variable::updateOrCreate(array('name'=>'popular3'),['value'=>request('unit3')]);

    	//get units to show as options
    	$units = Unit::latest()->get();

    	//get populars ids
    	$populars = Variable::where('name','like','%popular%')->get();

        \Session::flash('messages', [trans('admin.edit_success_msg')]);
        
    	return view('admin.popular',compact('units','populars'));
    }

    ////////////////////////////////////
    ////////////////////////////////////
    //////// //units ///////////////////
    ////////////////////////////////////
    public function all_units()
    {
        
        if(empty(request('query')))
        {
            $units = Unit::latest()->paginate(10);

        }
        else
        {
            
            $units = Unit::join('sections','units.section_id','=','sections.id')
            ->join('locations','units.location_id','=','locations.id')
            ->join('users','units.user_id','=','users.id')
            ->where('title','like','%'.request('query').'%')
            ->orWhere('name','like','%'.request('query').'%')
            ->orderBy('units.created_at','DESC')
            ->paginate(10);

        }
        return view('admin.all_units',compact('units'));
    }

    public function add_unit()
    {

        $sections   = Section::all();

        $locations  = json_encode(Location::all(),JSON_UNESCAPED_UNICODE);

        return view('admin.unit',compact('sections','locations'));
    }

    public function store_unit($lang,Request $req)
    {
        //dd($req->file('images'));

        //store unit in database
        $unit = auth()->user()->publish_unit(new Unit([

            'title'         => $req->title,
            'description'   => $req->description,
            'price'         => $req->price,
            'type'          => $req->type,
            'section_id'    => $req->section_id,
            'phone'         => $req->phone,
            'size'          => $req->size,
            'rooms'         => $req->rooms,
            'bathrooms'     => $req->bathrooms,
            'floor'         => $req->floor,
            'view'          => $req->view,
            'pay_type'      => $req->pay_type,
            'build_year'    => $req->build_year,
            'delivery_date' => $req->delivery_date,
            'location_id'   => $req->location_id,
            'address'       => $req->address,

        ]));
        //get request params
        $files  = request()->file('images');
        $inputs = request()->input();
        
        //loop in files and store them in public path
        foreach ($files as $key => $file) 
        {
            
            $tmp_file = new Upload($file);
            $extension = $tmp_file->ext;


            $tmp_file->move('uploads/units/'.$unit->id,$key.'.'.$extension);
        }
        $sections = Section::all();

        \Session::flash('messages', [trans('admin.edit_success_msg')]);

        return redirect()->route('admin_all_units',app('lang'));        
    }

    public function edit_unit($lang,Unit $unit)
    {

        $sections   = Section::all();

        $locations  = json_encode(Location::all(),JSON_UNESCAPED_UNICODE);

        //get all levels locations
        /* 
        $sel_level = $unit->Location->level;
        $sel_locations = array();
        $sel_locations[$sel_level] = $unit->Location;

        for($i=$sel_level-1; $i>=0; $i--)
        {
            $sel_locations[$i] = Location::find($sel_locations[$i+1]->parent_id);
        }
       */

        return view('admin.unit',compact('sections','locations','unit','sel_locations'));
    }

    public function update_unit($lang,Unit $unit,Request $req)
    {
        //dd($req->file('images'));

        //store unit in database
        $unit->update([

            'title'         => $req->title,
            'description'   => $req->description,
            'price'         => $req->price,
            'type'          => $req->type,
            'section_id'    => $req->section_id,
            'phone'         => $req->phone,
            'size'          => $req->size,
            'rooms'         => $req->rooms,
            'bathrooms'     => $req->bathrooms,
            'floor'         => $req->floor,
            'view'          => $req->view,
            'pay_type'      => $req->pay_type,
            'build_year'    => $req->build_year,
            'delivery_date' => $req->delivery_date,
            'location_id'   => $req->location_id,
            'address'       => $req->address,

        ]);
        $unit->save();

        //get request params
        $files  = request()->file('images');
        $inputs = request()->input();
        

        //check if there are new images
        if(!empty($files))
        {


            //delete old files
            if(\File::exists(public_path('uploads/units/'.$unit->id)))
            {

                \File::deletedirectory(public_path('uploads/units/'.$unit->id));
            }


            //loop in files and store them in public path

            foreach ($files as $key => $file) 
            {
                
                $tmp_file = new Upload($file);
                $extension = $tmp_file->ext;
                $tmp_file->move('uploads/units/'.$unit->id,$key.'.'.$extension);
            }
        }
        $sections = Section::all();

        \Session::flash('messages', [trans('admin.edit_success_msg')]);

        return redirect()->route('admin_all_units',app('lang'));        
    }

    public function delete_unit($lang,Unit $unit)
    {
        //delete old files
        if(\File::exists(public_path('uploads/units/'.$unit->id)))
        {

            \File::deletedirectory(public_path('uploads/units/'.$unit->id));
        }


        \Session::flash('messages', [trans('admin.del_success_msg')]);

        $unit -> delete();
        return back();
    }

    /////////////////////////////////////////////
    /////////////////////////////////////////////
    /////////// Services ////////////////////////
    /////////////////////////////////////////////

    public function all_services()
    {
        if(empty(request('query')))
        {
            $services = Service::latest()->paginate(10);
            
        }
        else
        {
            
            $services = Service::where('ar_title','like','%'.request('query').'%')
            ->orWhere('en_title','like','%'.request('query').'%')
            ->orWhere('ar_des','like','%'.request('query').'%')
            ->orWhere('en_des','like','%'.request('query').'%')
            ->orderBy('created_at','DESC')
            ->paginate(10);

        }
        return view('admin.all_services',compact('services'));
    }

    public function add_service()
    {

        return view('admin.service');
    }

    public function store_service(Request $req)
    {
        $service = auth()->user()->create_service(new Service([

            'en_title'      => $req->en_title,
            'ar_title'      => $req->ar_title,
            'en_des'        => $req->en_des,
            'ar_des'        => $req->ar_des,
            'icon'          => $req->icon,
        ]));

        \Session::flash('messages', [trans('admin.edit_success_msg')]);

        return redirect()->route('admin_all_services',app('lang'));
    }

    public function edit_service($lang,Service $service)
    {
        return view('admin.service',compact('service'));
    }

    public function update_service($lang,Service $service,Request $req)
    {
        $service->update([
            'en_title'      => $req->en_title,
            'ar_title'      => $req->ar_title,
            'en_des'        => $req->en_des,
            'ar_des'        => $req->ar_des,
            'icon'          => $req->icon,
        ]);
        \Session::flash('messages', [trans('admin.edit_success_msg')]);

        return redirect()->route('admin_all_services',app('lang'));
    }

    public function delete_service($lang,Service $service)
    {
        \Session::flash('messages', [trans('admin.del_success_msg')]);
        $service->delete();
        return back();
    }

    /////////////////////////////////////////////
    /////////////////////////////////////////////
    /////////// users ////////////////////////
    /////////////////////////////////////////////

    public function all_users()
    {
        if(empty(request('query')))
        {
            $users = User::latest()->paginate(10);
            
        }
        else
        {
            
            $users = User::Where('name','like','%'.request('query').'%')
            ->orWhere('email','like','%'.request('query').'%')
            ->orWhere('phone','like','%'.request('query').'%')
            ->orderBy('created_at','DESC')
            ->paginate(10);

        }
        return view('admin.all_users',compact('users'));
    }

    public function add_user()
    {

        return view('admin.user');
    }

    public function store_user(Request $req)
    {

        $this->validate($req,[

            'name'      => 'required|min:4',
            'phone'     => 'required|unique:users',
            'email'     => 'required|email|unique:users',
            'password'  => 'required|confirmed',
        ]);
        User::create([

            'name'      => $req->name,
            'phone'     => $req->phone,
            'email'     => $req->email,
            'password'  => bcrypt($req->password),
            'role'      => 'admin',
        ]);

        \Session::flash('messages', [trans('admin.edit_success_msg')]);

        return redirect()->route('admin_all_users',app('lang'));
    }

    public function edit_user($lang,User $user)
    {
        return view('admin.user',compact('user'));
    }

    public function update_user($lang,User $user,Request $req)
    {

        $this->validate($req,[

            'name'      => 'required|min:4',
            'phone'     => 'required',
            'email'     => 'required|email',
            'password'  => 'required|confirmed',
        ]);

        $user->update([

            'name'      => $req->name,
            'phone'     => $req->phone,
            'email'     => $req->email,
            'password'  => bcrypt($req->password),
        ]);

        \Session::flash('messages', [trans('admin.edit_success_msg')]);

        return redirect()->route('admin_all_users',app('lang'));


    }

    public function delete_user($lang,User $user)
    {
        if($user->role == 'owner')
        {
            return 'you can\'t delete the owner';
        }
        else
        {
            $user->delete();

            \Session::flash('messages', [trans('admin.del_success_msg')]);

            return back();
        }
        
    }

    ////////////////////////////////
    ///////// show inquiries //////
    //////////////////////////////
    public function get_inquires(Request $req)
    {
        if(empty(request('query')))
        {
            $inquiries = Inquiry::latest()->paginate(10);
            
        }
        else
        {
            
            $inquiries = Inquiry::join('units','inquiries.unit_id','=','units.id')
            ->where('units.title','like','%'.request('query').'%')
            ->orWhere('inquiries.name','like','%'.request('query').'%')
            ->orWhere('inquiries.phone','like','%'.request('query').'%')
            ->orWhere('inquiries.email','like','%'.request('query').'%')
            ->select('units.title','units.id','inquiries.*')
            ->orderBy('inquiries.created_at','DESC')
            ->paginate(10);
            
        }

        return view('admin.inquires',compact('inquiries'));
    }


    ///////////////////////////////////
    //////////// SMS /////////////////
    //////////////////////////////////

    public function send_sms()
    {       
        $sms_credit = Variable::where('name','sms_credit')->first();
        return view('admin.sms',compact('sms_credit'));
    }

    public function is_rtl( $string ) 
    {
        $rtl_chars_pattern = '/[\x{0590}-\x{05ff}\x{0600}-\x{06ff}]/u';
        return preg_match($rtl_chars_pattern, $string);
    }

    public function post_sms(Request $req)
    {

        $phone  = '2'.trim($req->phone);
        $sender = substr($req->phone,0,3) == '010' ? 'M Masr' : 'Meamar%20Masr';
        $msg    = $req->msg;
        

        $msg_lang = $this->is_rtl( $msg ) ? 2 : 1;
        $msg = urlencode($msg);
        $link = 'http://sms.zero1business.com/api/send/?username=USER_NAME&password=PASSWORD&language='.$msg_lang.'&sender='.$sender.'&mobile='.$phone.'&message='.$msg;
        

        //dd($link);

        //get response and replace code of response with message
        $res = str_replace(array('1901','1906','1902'), array(trans('admin.send_success_msg'),trans('admin.send_no_balance_msg'),trans('admin.send_error_msg')), file_get_contents($link));
        
        $match = preg_match('/credit:\\S*/', $res,$credit);
        $credit = explode(':',$credit[0])[1] or ''; 
        Variable::updateOrCreate(['name' => 'sms_credit' ],['value' => $credit]);
        
        if(strpos($res, trans('admin.send_success_msg')) > -1)
        {
            \Session::flash('messages', [$res]);
            return back();
        }
        else
        {
            return back()->withErrors([$res]);
        }
        
    }
}
