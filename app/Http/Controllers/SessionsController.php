<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['guest'])->except(['destroy']);
    }

    public function create(Request $req)
    {
       return view('admin.login');
    }
    
    public function store(Request $req)
    {
    	if($req->isMethod('post'))
    	{

	    	//validate inputs
	    	$this->validate($req,[
	    		'login_method'	=> 'required',
	    		'password'		=> 'required',
	    	]);

		   	//authenticate the user with email

	    	if(auth()->attempt(['email' => $req->login_method , 'password' => $req->password]))
	    	{
	    		//logged with email
	    		return redirect()->route('admin_home',app('lang'));
		    }
		    elseif(auth()->attempt(['phone' => $req->login_method , 'password' => $req->password]))
		    {

    			//logged with phone
	    		return redirect()->route('admin_home',app('lang'));
		    }
			else
			{
				return back()->withErrors([

					'message' => trans('sessions.loginErr'),
				]);
			}
		}
    }
    //logout
    public function destroy()
    {
    	auth()->logout();
    	return redirect(route('login'));
    }

}
