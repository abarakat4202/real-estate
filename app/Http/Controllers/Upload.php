<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Upload extends Controller
{
    //
    public $file;
    public $name; 
    public $ext; 
    public $size; 
    public $mim; 
    public $r_path;

    public function __construct($file)
    {
    	return $this->set_file($file);
    }

	public function set_file($file)
	{
		$this->file  =  $file;

        $this->name   = $file->getClientOriginalName();
        $this->ext    = $file->getClientOriginalExtension();
        $this->size   = $file->getSize();
        $this->mim    = $file->getMimeType();
        $this->r_path = $file->getRealPath();
        
        
        
    }

    public function move($path,$name)
    {
    	
    	//upload file to 
    	$this->file->move(public_path($path),$name);
    }
}
