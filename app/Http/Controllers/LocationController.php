<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Location;

class LocationController extends Controller
{
    //
    public function store(Request $req)
    {
    	$this -> validate(request(),[
    		'slug' => 'unique:locations',
    	]);
    	$location = new Location;

    	$location -> ar_title 	= request('ar_title');
    	$location -> slug 		= request('slug');
    	$location -> parent_slug= request('parent');
    	$location -> lat 		= request('lat');
    	$location -> lng 		= request('lng');
    	$location -> zoom_level = request('zoom_level');
    	$location -> searchable = request('searchable');
    	$location -> estimate 	= request('estimate');
    	$location -> level 		= request('level');
    	$location -> parent_id 	= request('parent_id');

    	$location -> save();
    }

    public function update_en(Request $req)
    {

    	$location = Location::where('slug',request('slug'))->first();

    	//$location['parent_slug'];
    	$location -> en_title 	= request('en_title');


    	$location -> save();
    }

    public function update_parent_id()
    {
    	$locations = Location::where('parent_slug','!=','no_parent')->get();
    	
    	$indx = 0;

    	foreach($locations as $location)
    	{
            //dd($location->parent_id);

	    		$parent_slug = $location->parent_slug;

	    		$parent_id 	 = Location::where('slug',$parent_slug)->first()->id;

	    		if($parent_id > 1)
	    		{
                    
		    		$location-> parent_id = $parent_id;

		    		$location->save();
                    echo $parent_id.':'.$location-> parent_id .'<br>';
		    	}

    		
    	}

    	return $parent_id.':'.$location-> parent_id;
    }

}
