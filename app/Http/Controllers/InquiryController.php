<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Unit;
use App\Inquiry;
class InquiryController extends Controller
{
    //
    public function create($lang,Request $req,Unit $unit)
    {

        

    	$this->validate(request(),[

    		'name' 	=> 'required',
    		'phone'	=> 'required',
    	
    	]);


    	Inquiry::create([

    		'name'			=> request('name'),
    		'phone'			=> request('phone'),
    		'email'			=> request('email'),
    		'message'		=> request('message'),
    		'unit_id'		=> $unit->id,
    		'inquiry_type'	=> 'unit',
    	]);
    	\Session::flash('messages', [trans('app.unit_applied_msg')]);
    	return back();
    	
    }

    public function contact($lang,Request $req)
    {


        $this->validate(request(),[

            'name'  => 'required',
            'phone' => 'required',
        
        ]);


        Inquiry::create([

            'name'          => request('name'),
            'phone'         => request('phone'),
            'email'         => request('email'),
            'message'       => request('message'),
            'inquiry_type'  => 'contact',
        ]);
        \Session::flash('messages', [trans('app.unit_applied_msg')]);
        return redirect()->route('home',app('lang'));
        
    }


}
