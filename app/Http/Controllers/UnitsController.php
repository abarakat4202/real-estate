<?php

namespace App\Http\Controllers;


use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Unit;
use App\User;
use App\Variable;
use App\Service;
use App\Location;
class UnitsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        
        $settings = array(
            //get main settings
            'logo_img'             => Variable::where('name','like','%logo_img%')->first(),
            'ar_title'             => Variable::where('name','like','%website_ar_title%')->first(),
            'en_title'             => Variable::where('name','like','%website_en_title%')->first(),
            'phone'                => Variable::where('name','like','%phone%')->first(),
            'ar_address'              => Variable::where('name','like','%ar_address%')->first(),
            'en_address'              => Variable::where('name','like','%en_address%')->first(),
            'fb'                   => Variable::where('name','like','%fb%')->first(),
        );

        // Sharing is caring
        View()->share('settings', $settings);
    }
    public function index()
    {

        //get sliders
        $slider1 = array(
            'ar_title' => Variable::where('name','slider1_ar_title')->first()->value,
            'en_title' => Variable::where('name','slider1_en_title')->first()->value,
            'ar_des'   => Variable::where('name','slider1_ar_des')->first()->value,
            'en_des'   => Variable::where('name','slider1_en_des')->first()->value,
            'img'      => Variable::where('name','slider1_img')->first()->value,
        );
        $slider2 = array(
            'ar_title' => Variable::where('name','slider2_ar_title')->first()->value,
            'en_title' => Variable::where('name','slider2_en_title')->first()->value,
            'ar_des'   => Variable::where('name','slider2_ar_des')->first()->value,
            'en_des'   => Variable::where('name','slider2_en_des')->first()->value,
            'img'      => Variable::where('name','slider2_img')->first()->value,
        );      
        $slider3 = array(
            'ar_title' => Variable::where('name','slider3_ar_title')->first()->value,
            'en_title' => Variable::where('name','slider3_en_title')->first()->value,
            'ar_des'   => Variable::where('name','slider3_ar_des')->first()->value,
            'en_des'   => Variable::where('name','slider3_en_des')->first()->value,
            'img'      => Variable::where('name','slider3_img')->first()->value,
        );        
             
        //get most populars
        $populars = Variable::where('name','like','%popular%')->get();
        $services = Service::Latest();
       
        $slogan = array(

            'en_title' => Variable::where('name','slogan_en_title')->first()->value,
            'ar_title' => Variable::where('name','slogan_ar_title')->first()->value,
            'ar_des'   => Variable::where('name','slogan_ar_des')->first()->value,
            'en_des'   => Variable::where('name','slogan_en_des')->first()->value,
            'img'      => Variable::where('name','slogan_img')->first()->value,
        );

        $partners_imgs = [

            'partner1'    => Variable::where('name','partner1_img')->first()->value,
            'partner2'    => Variable::where('name','partner2_img')->first()->value,
            'partner3'    => Variable::where('name','partner3_img')->first()->value,
            'partner4'    => Variable::where('name','partner4_img')->first()->value,
            'partner5'    => Variable::where('name','partner5_img')->first()->value,
        ];

        $locations  = json_encode(Location::all(),JSON_UNESCAPED_UNICODE);

        $home = 1;

        return view('real_home.index',compact('populars','services','partners_imgs','slogan','home','slider1','slider2','slider3','locations'));
    }

    public function all(Request $req)
    {
        //check if request has inputs
        if($location = $req->location_1)
        {
            //filter by price
            $units = Unit::where('price','>=',$req->price_from)->where('price','<=',$req->price_to);
            //filter by location
            $units = $units->paginate()->filter(function($unit) use ($location)
            {
             return in_array($location,$unit->location_ids());
            });
           $units = array_column(array_values($units->toArray()),'id');
           $units = Unit::whereIn('id',$units)->paginate(30);

        }
        else
        {
            $units = Unit::latest()->paginate(30);
        }
        return view('real_home.units.all',compact('units'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function show($lang,Unit $unit)
    {
        //get realted units
        $related = $unit->related();


     
        //get latest units
        $latest = Unit::latest()->get();

       //dd($related[0]->img_url(0));

        return view('real_home.units.show',compact('unit','related','latest'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function edit($lang,Unit $unit)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Unit $unit)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Unit $unit)
    {
        //
    }

    public function about()
    {

        $arr = [
            'en_title'      => Variable::where('name','about_en_title')->first()->value,
            'ar_title'      => Variable::where('name','about_ar_title')->first()->value,
            'en_des'        => Variable::where('name','about_en_des')->first()->value,
            'ar_des'        => Variable::where('name','about_ar_des')->first()->value,
            'img'           => Variable::where('name','about_img')->first()->value,

        ];
        return view('real_home.about',compact('arr'));
    }

    public function contact()
    {
        $arr = [
            'en_title'      => Variable::where('name','about_en_title')->first()->value,
            'ar_title'      => Variable::where('name','about_ar_title')->first()->value,
            'en_des'        => Variable::where('name','about_en_des')->first()->value,
            'ar_des'        => Variable::where('name','about_ar_des')->first()->value,
            'img'           => Variable::where('name','about_img')->first()->value,

        ];
        return view('real_home.contact',compact('arr'));
    }
}
