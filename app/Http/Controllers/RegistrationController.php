<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class RegistrationController extends Controller
{
    //
    public function __construct()
    {
    	return $this->middleware('guest')->except([]);
    }

    public function create()
    {	
    	//return view();
    }

    public function store(Request $req)
    {
    	//validate registration form
    	$this->validate(request(),[

    		'name' 		=> 'required|min:4',
    		'email' 	=> 'required|email',
    		'phone' 	=> 'required',
    		'password' 	=> 'required|confirmed',
    	]);

    	//create and save the new user
    	$user = User::create(
    		'name' 		=> $req->name,
    		'email' 	=> $req->email,
    		'phone' 	=> $req->phone,
    		'password' 	=> $req->password,
    	);
    }

}
