<?php

namespace App\Http\Middleware;

use Closure;

class Lang
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(in_array(request()->segment(1),['ar','en']))
        {
            //change app lang 
            app()->singleton('lang',function(){ return request()->segment(1);});
        }

            app()->setLocale(app('lang'));
        
        return $next($request);
    }
}
