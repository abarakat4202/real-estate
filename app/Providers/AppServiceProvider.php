<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //create singleton of language
        app()->singleton('lang',function(){

            if(in_array(Request()->segment(1),['ar,en']))
            {
                    return Request()->segment(1);
            }
            else
            {

                if(auth()->user())
                {
                    if(empty(auth()->user()->lang))
                    {
                        return 'en';
                    }
                    else
                    {
                        return auth()->user()->lang;
                    }
                }
                else
                {
                    if(session()->has('lang'))
                    {
                        return session()->get('lang');
                    }
                    else
                    {
                        return 'en';
                    }
                }
            }
        

        });

        //string length
        Schema::defaultStringlength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
