<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    //
    protected $guard = [];

    public function units()
    {
        return $this->hasMany(Unit::class)->orderBy('created_at')->get();
    }
}
