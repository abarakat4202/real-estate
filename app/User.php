<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    /*protected $fillable = [
        'name', 'email', 'password',
    ];*/

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $guarded = [];
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $guard = ['remember_token'];

    public function units()
    {
        return $this->hasMany(Unit::class);
    }

    public function services()
    {
        return $this->hasMany(Service::class);
    }    
    public function publish_unit(Unit $unit)
    {
        return $this->units()->save($unit);
    }

    public function create_service(Service $service)
    {
        return $this->services()->save($service);
    }
}
