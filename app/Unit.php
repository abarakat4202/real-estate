<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    //
	protected $guarded = [];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }


    public function location()
    {
    	return $this->belongsTo(Location::class);
    }

    public function location_tree()
    {
        //get all levels locations
        $sel_level = $this->Location->level;
        $arr = array();
        $arr[$sel_level] = $this->Location;


        for($i=$sel_level-1; $i>=0; $i--)
        {

            $arr[$i] = Location::find($arr[$i+1]->parent_id);
        }
       
       $arr = array_reverse($arr);
        foreach ($arr as $key => $row) {
            $tree[] = $row[app('lang').'_title'];
        }
     
        
        return $tree;
    }
    public function location_ids()
    {
        //get all levels locations
        $sel_level = $this->Location->level;
        $arr = array();
        $arr[$sel_level] = $this->Location;


        for($i=$sel_level-1; $i>=0; $i--)
        {

            $arr[$i] = Location::find($arr[$i+1]->parent_id);
        }
       
       $arr = array_reverse($arr);
        foreach ($arr as $key => $row) {
            $tree[] = $row['id'];
        }
     
        
        return $tree;
    }
    public function inquiries()
    {
        return $this->hasMany(Inquiry::class);
    }

    public function section()
    {
        return $this->belongsTo(Section::class);
    }


    public function related()
    {
        $unit_id = $this->id;
        $location_id = $this->location->id;
        $parent_id = $this->location->parent_id;

        //get second level locations to match units that have the same locaion id in this level
        $second_level_location_id = $this->location_tree()[1];

    	//return $location_id;
    	return static::selectRaw('units.*')
        ->join('locations','locations.id','=','units.location_id')
        ->Where('locations.parent_id',$parent_id)
        ->Where('units.id','!=',$unit_id)
        ->orderBy('units.created_at','DESC')
        ->limit(5)
        ->get();
    }



    public function slug()
    {
        return str_slug($this->title,'-');
    }

    public function url()
    {
        return route('unit_view',[app('lang'),$this->id,$this->slug()]);
    }



    public function images()
    {
        
        if(\File::exists(public_path('uploads/units/'.$this->id)))
        {

            return \File::allFiles(public_path('uploads/units/'.$this->id));
        }
        else
        {
            return [];
        }

       
    }

    public function img_url($ind)
    {
        if(!empty($this->images()[$ind]))
        {

            return asset('/uploads/units/'.$this->id.'/'.basename($this->images()[$ind]));
        }
        else
        {
            return;
        }
    }
}
