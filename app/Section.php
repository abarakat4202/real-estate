<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    //
    protected $guarded = [];
    function units()
    {
    	return $this->hasMany(Unit::class);
    }
}
