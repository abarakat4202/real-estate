<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Variable extends Model
{
    //
    protected $fillable = ['name','value'];

    //protected $guard = [];

    //get most populars units
    public function unit()
    {
    	return $this->hasOne(Unit::class,'id','value');
    }


}
